package ch.usi.inf.saiv.railwayempire.model.cells;

import org.junit.Assert;
import org.junit.Test;

public class TestCell {

    @Test
    public void testCellCreation() {
        ICell testCell = new GroundCell(5);
        Assert.assertNotNull("[FAILED] Cell shouldn't be null", testCell);
        Assert.assertNotNull("[FAILED] Height of the cell shouldn't be null", testCell.getHeight());
        Assert.assertEquals("[FAILED] The height of the cell should be 10", testCell.getHeight(), 5);
        testCell = new SeaCell(5);
        Assert.assertNotNull("[FAILED] Cell shouldn't be null", testCell);
        Assert.assertNotNull("[FAILED] Height of the cell shouldn't be null", testCell.getHeight());
        Assert.assertEquals("[FAILED] The height of the cell should be 10", testCell.getHeight(), 5);
        testCell = new RiverCell(5);
        Assert.assertNotNull("[FAILED] Cell shouldn't be null", testCell);
        Assert.assertNotNull("[FAILED] Height of the cell shouldn't be null", testCell.getHeight());
        Assert.assertEquals("[FAILED] The height of the cell should be 10", testCell.getHeight(), 5);
        testCell = new MountainCell(5);
        Assert.assertNotNull("[FAILED] Cell shouldn't be null", testCell);
        Assert.assertNotNull("[FAILED] Height of the cell shouldn't be null", testCell.getHeight());
        Assert.assertEquals("[FAILED] The height of the cell should be 5", testCell.asMountainCell().getRealHeight(), 5);
        testCell = new MountainCell(6);
        Assert.assertNotNull("[FAILED] Cell shouldn't be null", testCell);
        Assert.assertNotNull("[FAILED] Height of the cell shouldn't be null", testCell.getHeight());
        Assert.assertEquals("[FAILED] The height of the cell should be 5", testCell.asMountainCell().getRealHeight(), 6);
        //testing mountain cell creating with too small and too high inputs
        testCell = new MountainCell(1);
        Assert.assertNotNull("[FAILED] Cell shouldn't be null", testCell);
        Assert.assertNotNull("[FAILED] Height of the cell shouldn't be null", testCell.getHeight());
        Assert.assertEquals("[FAILED] The height of the cell should be 5", testCell.asMountainCell().getRealHeight(), 5);
        testCell = new MountainCell(9);
        Assert.assertNotNull("[FAILED] Cell shouldn't be null", testCell);
        Assert.assertNotNull("[FAILED] Height of the cell shouldn't be null", testCell.getHeight());
        Assert.assertEquals("[FAILED] The height of the cell should be 5", testCell.asMountainCell().getRealHeight(), 6);
    }

    @Test
    public void testIsBlocked() {
        final GroundCell groundCell = new GroundCell(10);
        Assert.assertTrue("[FAILED] GroundCell shouldn't be blocked", groundCell.canBuildBuilding());
        final SeaCell seaCell = new SeaCell(10);
        Assert.assertFalse("[FAILED] SeaCell shouldn be blocked", seaCell.canBuildBuilding());
        final RiverCell riverCell = new RiverCell(10);
        Assert.assertFalse("[FAILED] RiverCell should be blocked", riverCell.canBuildBuilding());
        final MountainCell mountainCell = new MountainCell(10);
        Assert.assertFalse("[FAILED] MountainCell should be blocked", mountainCell.canBuildBuilding());
    }

    @Test
    public void testIsTraversable() {
        final GroundCell groundCell = new GroundCell(10);
        Assert.assertTrue("[FAILED] GroundCell should be traversable", groundCell.canBuildTrack());
        Assert.assertTrue("[FAILED] GroundCell should be allowed to build structures", groundCell.canBuildBuilding());
        //deep sea cell - cant build bridge
        final SeaCell seaCell = new SeaCell(5);
        Assert.assertFalse("[FAILED] SeaCell depth 5 shouldn't be traversable", seaCell.canBuildTrack());
        Assert.assertFalse("[FAILED] SeaCell depth 5 shouldn't be allowed to build structures", seaCell.canBuildBuilding());
        //shallow sea cell - can build bridge
        final SeaCell seaBridgeCell = new SeaCell(8);
        Assert.assertTrue("[FAILED] SeaCell depth 1 should be traversable", seaBridgeCell.canBuildTrack());
        Assert.assertFalse("[FAILED] SeaCell depth 1 shouldn't be allowed to build structures", seaBridgeCell.canBuildBuilding());
        final RiverCell riverCell = new RiverCell(10);
        Assert.assertTrue("[FAILED] RiverCell should be traversable", riverCell.canBuildTrack());
        Assert.assertFalse("[FAILED] RiverCell should be traversable", riverCell.canBuildBuilding());
        final MountainCell mountainCell = new MountainCell(10);
        Assert.assertTrue("[FAILED] MountainCell should be traversable", mountainCell.canBuildTrack());
        Assert.assertFalse("[FAILED] MountainCell should be traversable", mountainCell.canBuildBuilding());
    }

    public void testCellType() {
        final GroundCell groundCell = new GroundCell(10);
        Assert.assertEquals("[FAILED] GroundCell should return CellTypes.GROUND", CellTypes.GROUND, groundCell.getCellType());
        final SeaCell seaCell = new SeaCell(10);
        Assert.assertEquals("[FAILED] GroundCell should return CellTypes.GROUND", CellTypes.SEA, seaCell.getCellType());
        final RiverCell riverCell = new RiverCell(10);
        Assert.assertEquals("[FAILED] GroundCell should return CellTypes.GROUND", CellTypes.RIVER, riverCell.getCellType());
        final MountainCell mountainCell = new MountainCell(10);
        Assert.assertEquals("[FAILED] GroundCell should return CellTypes.MOUNTAIN", CellTypes.MOUNTAIN, mountainCell.getCellType());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNegativeGroundCellheight() {
        new GroundCell(-1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNegativeSeaCellheight() {
        new SeaCell(-1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNegativeRiverCellheight() {
        new RiverCell(-1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNegativeMountainCellheight() {
        new MountainCell(-1);
    }

}
