package ch.usi.inf.saiv.railwayempire.model.trains;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import ch.usi.inf.saiv.railwayempire.model.Game;
import ch.usi.inf.saiv.railwayempire.model.structures.Station;
import ch.usi.inf.saiv.railwayempire.model.structures.StationSizes;
import ch.usi.inf.saiv.railwayempire.utilities.WorldSettings;

public class TestStation {

    @Test
    public void testStationCreation() {
        Game.getInstance().generateWorld(new WorldSettings());
        final Station smallStation = new Station(0, 0, StationSizes.SMALL);
        assertEquals(0, smallStation.getX());
        assertEquals(0, smallStation.getY());
        assertEquals(StationSizes.SMALL, smallStation.getSize());

        final Station mediumStation = new Station(0, 0, StationSizes.MEDIUM);
        assertEquals(0, mediumStation.getX());
        assertEquals(0, mediumStation.getY());
        assertEquals(StationSizes.MEDIUM, mediumStation.getSize());

        final Station largeStation = new Station(0, 0, StationSizes.LARGE);
        assertEquals(0, largeStation.getX());
        assertEquals(0, largeStation.getY());
        assertEquals(StationSizes.LARGE, largeStation.getSize());
    }
}
