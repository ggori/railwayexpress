package ch.usi.inf.saiv.railwayempire.model;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import ch.usi.inf.saiv.railwayempire.model.cells.GroundCell;
import ch.usi.inf.saiv.railwayempire.model.cells.ICell;
import ch.usi.inf.saiv.railwayempire.model.cells.MountainCell;
import ch.usi.inf.saiv.railwayempire.model.cells.RiverCell;
import ch.usi.inf.saiv.railwayempire.model.cells.SeaCell;
import ch.usi.inf.saiv.railwayempire.model.production_sites.Building;
import ch.usi.inf.saiv.railwayempire.model.production_sites.IProductionSite;
import ch.usi.inf.saiv.railwayempire.model.production_sites.PostOffice;
import ch.usi.inf.saiv.railwayempire.model.structures.Forest;
import ch.usi.inf.saiv.railwayempire.model.structures.Station;
import ch.usi.inf.saiv.railwayempire.model.structures.StationSizes;
import ch.usi.inf.saiv.railwayempire.model.structures.Track;
import ch.usi.inf.saiv.railwayempire.utilities.WorldSettings;

public class TestTerrain {
    
    @Test
    public void testCoordinatesInRange() {
        final Terrain terrain = new Terrain(100, 100);
        Assert.assertTrue("The coordinates should be in range", terrain.areCoordinatesInRange(50, 50));
        Assert.assertTrue("The coordinates should be in range", terrain.areCoordinatesInRange(99, 50));
        Assert.assertTrue("The coordinates should be in range", terrain.areCoordinatesInRange(0, 50));
        Assert.assertTrue("The coordinates should be in range", terrain.areCoordinatesInRange(50, 0));
        Assert.assertTrue("The coordinates should be in range", terrain.areCoordinatesInRange(50, 99));
        Assert.assertFalse("The coordinates shouldn't be in range", terrain.areCoordinatesInRange(-1, 50));
        Assert.assertFalse("The coordinates shouldn't be in range", terrain.areCoordinatesInRange(100, 50));
        Assert.assertFalse("The coordinates shouldn't be in range", terrain.areCoordinatesInRange(50, 100));
        Assert.assertFalse("The coordinates shouldn't be in range", terrain.areCoordinatesInRange(50, -1));
        Assert.assertFalse("The coordinates shouldn't be in range", terrain.areCoordinatesInRange(-1, 100));
        
    }
    
    @Test
    public void testTerrainSize() {
        final Terrain terrain = new Terrain(100,100);
        Assert.assertEquals("Size must be 100", 100, terrain.getSize());
    }
    
    @Test
    public void testAddAndGetCell() {
        final Terrain terrain = new Terrain(500,500);
        final ICell cell  = new GroundCell(1);
        terrain.addCell(10, 8, cell);
        Assert.assertEquals("The cell should be equals to the one we created", cell, terrain.getCell(10, 8));
    }
    
    @Test
    public void testCanBuildStation() {
        final int size = 100;
        final Terrain terrain = new Terrain(size, size);
        for (int y = 0; y < size; ++y) {
            for (int x = 0; x < size; ++x) {
                terrain.addCell(x, y, new GroundCell(1));
            }
        }
        Assert.assertTrue(terrain.canBuildStation(10, 10, 5));
        Assert.assertTrue(terrain.canBuildStation(10, 10, 15));
        
        terrain.addCell(5, 5, new SeaCell(2));
        Assert.assertFalse(terrain.canBuildStation(5, 5, 5));
        
        terrain.addCell(8, 8, new RiverCell(2));
        Assert.assertFalse(terrain.canBuildStation(8, 8, 5));
        
        terrain.setCell(10, 10, new MountainCell(6));
        Assert.assertFalse(terrain.canBuildStation(10, 10, 5));
        
        terrain.getCell(20, 20).setStructure(new Forest(20, 20, 0));
        Assert.assertTrue(terrain.canBuildStation(20, 20, 5));
        
        Game.getInstance().generateWorld(new WorldSettings());
        terrain.markCellsAsOwned(10, 10, 10, new Station(10, 10, StationSizes.SMALL));
        Assert.assertFalse(terrain.canBuildStation(15, 5, 15));
        
        Assert.assertTrue(terrain.canBuildStation(40, 40, 15));
        
        terrain.getCell(60, 60).setStructure(new Track(60, 60));
        Assert.assertFalse(terrain.canBuildStation(60, 60, 5));
        
        final City newCity = new City(80, 80);
        terrain.getCell(80, 80).setStructure(new Building(80, 80, 100, 100, newCity));
        Assert.assertFalse(terrain.canBuildStation(80, 80, 5));
    }
    
    @Test
    public void testExtractionSites() {
        final int size = 100;
        final Terrain terrain = new Terrain(size, size);
        for (int y = 0; y < size; ++y) {
            for (int x = 0; x < size; ++x) {
                terrain.addCell(x, y, new GroundCell(1));
            }
        }
        final PostOffice postOffice = new PostOffice(10, 10);
        terrain.getCell(5, 5).setStructure(postOffice);
        
        final List<IProductionSite> productionSites = terrain.getExtractionSites(10, 10, 10);
        Assert.assertEquals(postOffice, productionSites.get(0));
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testAddAndGetInvalidCell() {
        final Terrain terrain = new Terrain(20,20);
        final ICell cell  = new GroundCell(1);
        terrain.addCell(10, 8, cell);
        terrain.getCell(21, 10);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testAddInvalidCellXNegative() {
        final Terrain terrain = new Terrain(20,20);
        final ICell cell  = new GroundCell(1);
        terrain.addCell(-1, 8, cell);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testAddInvalidCellYNegative() {
        final Terrain terrain = new Terrain(20,20);
        final ICell cell  = new GroundCell(1);
        terrain.addCell(1, -8, cell);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testAddInvalidCellXOutOfBoundaries() {
        final Terrain terrain = new Terrain(20,20);
        final ICell cell  = new GroundCell(1);
        terrain.addCell(22, 10, cell);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testAddInvalidCellYOutOfBoundaries() {
        final Terrain terrain = new Terrain(20,20);
        final ICell cell  = new GroundCell(1);
        terrain.addCell(8, 30, cell);
    }
    
    
    
    
}
