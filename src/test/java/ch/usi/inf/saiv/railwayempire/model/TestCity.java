package ch.usi.inf.saiv.railwayempire.model;

import org.junit.Test;

import static org.junit.Assert.*;

public class TestCity {

	@Test
	public void testCityCreation() {
		City city = new City(0,0);
		assertEquals( "Coordinate x should be set corrected", 0, city.getX());
		assertEquals( "Coordinate y should be set corrected", 0, city.getY());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testNegativeXCoord () {
		new City(-1,0);		
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testNegativeYCoord () {
		new City(0,-1);		
	}
}
