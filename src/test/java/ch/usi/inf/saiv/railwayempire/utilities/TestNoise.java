package ch.usi.inf.saiv.railwayempire.utilities;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

/**
 *
 */
public class TestNoise {
		
	@Test
	public void testNoiseCreation() {
		Noise noise = new Noise(16);
		assertNotNull("[FAILED] Noise shouldn't be null", noise);
	}
	
	@Test
	public void testGetSize() {
		Noise noise = new Noise(16);
		assertEquals("[FAILED] Noise should have the given size", 16, noise.getSize());	
	}
	
	@Test
	public void testGetSetValue() {
		Noise noise = new Noise(16);
		assertNotNull("[FAILED] Value shouldn't be null", noise.getValue(4,4));
		//noise.setValue(4, 4, 0.5);
		//assertEquals("[FAILED] Value should be the one given", 0.5, noise.getValue(4,4));
	}
	
	
	
	@Test(expected = IllegalArgumentException.class)
	public void testNegativeSizeNoise() {
		new Noise(-1);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testNotPowerOfTwoNoise() {
		new Noise(3);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testBigXGetValue() {
		Noise noise = new Noise(16);
		noise.getValue(20, 4);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testBigYGetValue() {
		Noise noise = new Noise(16);
		noise.getValue(4, 20);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testNegativeXGetValue() {
		Noise noise = new Noise(16);
		noise.getValue(-1, 4);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testNegativeYGetValue() {
		Noise noise = new Noise(16);
		noise.getValue(4, -1);
	}
	/*@Test(expected = IllegalArgumentException.class)
	public void testBigXSetValue() {
		Noise noise = new Noise(16);
		noise.setValue(20, 4, 0.5);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testBigYSetValue() {
		Noise noise = new Noise(16);
		noise.setValue(4, 20, 0.5);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testNegativeXsetValue() {
		Noise noise = new Noise(16);
		noise.setValue(-1, 4, 0.5);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testNegativeYSetValue() {
		Noise noise = new Noise(16);
		noise.setValue(4, -1, 0.5);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testNegativeNoiseSetValue() {
		Noise noise = new Noise(16);
		noise.setValue(4, 4, -1);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testBigNoiseSetValue() {
		Noise noise = new Noise(16);
		noise.setValue(4,4,2);
	}*/
}
