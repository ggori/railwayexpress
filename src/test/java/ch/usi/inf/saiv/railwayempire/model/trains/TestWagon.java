package ch.usi.inf.saiv.railwayempire.model.trains;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import ch.usi.inf.saiv.railwayempire.model.Game;
import ch.usi.inf.saiv.railwayempire.model.production_sites.CoalMine;
import ch.usi.inf.saiv.railwayempire.model.production_sites.Farm;
import ch.usi.inf.saiv.railwayempire.model.production_sites.IronMine;
import ch.usi.inf.saiv.railwayempire.model.production_sites.OilRig;
import ch.usi.inf.saiv.railwayempire.model.production_sites.PostOffice;
import ch.usi.inf.saiv.railwayempire.model.production_sites.RockCave;
import ch.usi.inf.saiv.railwayempire.model.structures.Station;
import ch.usi.inf.saiv.railwayempire.model.structures.StationSizes;
import ch.usi.inf.saiv.railwayempire.model.wagons.CoalWagon;
import ch.usi.inf.saiv.railwayempire.model.wagons.FoodWagon;
import ch.usi.inf.saiv.railwayempire.model.wagons.IWagon;
import ch.usi.inf.saiv.railwayempire.model.wagons.IronWagon;
import ch.usi.inf.saiv.railwayempire.model.wagons.MailWagon;
import ch.usi.inf.saiv.railwayempire.model.wagons.OilWagon;
import ch.usi.inf.saiv.railwayempire.model.wagons.PassengersWagon;
import ch.usi.inf.saiv.railwayempire.model.wagons.RockWagon;
import ch.usi.inf.saiv.railwayempire.model.wagons.WoodWagon;
import ch.usi.inf.saiv.railwayempire.utilities.GameConstants;
import ch.usi.inf.saiv.railwayempire.utilities.Time;
import ch.usi.inf.saiv.railwayempire.utilities.WorldSettings;

public class TestWagon {
    
    private List<IWagon> wagons;
    private IronWagon ironWagon;
    private FoodWagon foodWagon;
    private PassengersWagon passengersWagon;
    private CoalWagon coalWagon;
    private MailWagon mailWagon;
    private WoodWagon woodWagon;
    private OilWagon oilWagon;
    private RockWagon rockWagon;
    
    @Before
    public void initWagons() {
        Game.getInstance().generateWorld(new WorldSettings());
        this.wagons = new ArrayList<IWagon>();
        this.ironWagon = new IronWagon("Iron Wagon", 10, 20, 4);
        this.foodWagon = new FoodWagon("Coal Wagon", 10, 20, 4);
        this.passengersWagon = new PassengersWagon("Passenger Wagon", 10, 20, 4);
        this.coalWagon = new CoalWagon("Coal Wagon", 10, 20, 4);
        this.mailWagon = new MailWagon("Mail Wagon", 10, 20, 4);
        this.woodWagon = new WoodWagon("Wood Wagon", 10, 20, 4);
        this.oilWagon = new OilWagon("Oil Wagon", 10, 20, 4);
        this.rockWagon = new RockWagon("Rock Wagon", 10, 20, 4);
        
        this.wagons.add(this.ironWagon);
        this.wagons.add(this.foodWagon);
        this.wagons.add(this.passengersWagon);
        this.wagons.add(this.coalWagon);
        this.wagons.add(this.mailWagon);
        this.wagons.add(this.woodWagon);
        this.wagons.add(this.oilWagon);
        this.wagons.add(this.rockWagon);
    }
    
    @Test
    public void testLoadMail() {
        Assert.assertEquals(0, this.mailWagon.getLoadedAmount(), 0);
        
        final Station station = new Station(15, 16, StationSizes.LARGE);
        
        final PostOffice postOffice = new PostOffice(13, 14);
        
        station.addProductionSites(postOffice);
        
        Assert.assertEquals(0, postOffice.getAmount(), 0);
        
        Time.getInstance().step(1001);
        
        Assert.assertEquals(GameConstants.POST_OFFICE_PRODUCTION_RATE, postOffice.getAmount(), 0);
        
        station.trade(this.wagons);
        
        Assert.assertEquals(GameConstants.POST_OFFICE_PRODUCTION_RATE, this.mailWagon.getLoadedAmount(), 0);
        
        Assert.assertEquals(0, this.ironWagon.getLoadedAmount(), 0);
        Assert.assertEquals(0, this.foodWagon.getLoadedAmount(), 0);
        Assert.assertEquals(0, this.passengersWagon.getLoadedAmount(), 0);
        Assert.assertEquals(0, this.coalWagon.getLoadedAmount(), 0);
        Assert.assertEquals(0, this.woodWagon.getLoadedAmount(), 0);
        Assert.assertEquals(0, this.oilWagon.getLoadedAmount(), 0);
        Assert.assertEquals(0, this.rockWagon.getLoadedAmount(), 0);
        Assert.assertEquals(0, this.foodWagon.getLoadedAmount(), 0);
        
        Assert.assertEquals(0, postOffice.getAmount(), 0);
    }
    
    @Test
    public void testLoadFood() {
        Assert.assertEquals(0, this.foodWagon.getLoadedAmount(), 0);
        
        
        final Station station = new Station(15, 16, StationSizes.LARGE);
        
        final Farm farm = new Farm(13, 14);
        
        station.addProductionSites(farm);
        
        Assert.assertEquals(0, farm.getAmount(), 0);
        
        Time.getInstance().step(1001);
        
        Assert.assertEquals(GameConstants.FARM_PRODUCTION_RATE, farm.getAmount(), 0);
        
        station.trade(this.wagons);
        
        Assert.assertEquals(GameConstants.FARM_PRODUCTION_RATE, this.foodWagon.getLoadedAmount(), 0);
        
        Assert.assertEquals(0, this.ironWagon.getLoadedAmount(), 0);
        Assert.assertEquals(0, this.passengersWagon.getLoadedAmount(), 0);
        Assert.assertEquals(0, this.coalWagon.getLoadedAmount(), 0);
        Assert.assertEquals(0, this.mailWagon.getLoadedAmount(), 0);
        Assert.assertEquals(0, this.woodWagon.getLoadedAmount(), 0);
        Assert.assertEquals(0, this.oilWagon.getLoadedAmount(), 0);
        Assert.assertEquals(0, this.rockWagon.getLoadedAmount(), 0);
        
        
        Assert.assertEquals(0, farm.getAmount(), 0);
    }
    
    @Test
    public void testLoadCoal() {
        Assert.assertEquals(0, this.coalWagon.getLoadedAmount(), 0);
        
        
        final Station station = new Station(15, 16, StationSizes.LARGE);
        
        final CoalMine coalMine = new CoalMine(13, 14);
        
        station.addProductionSites(coalMine);
        
        Assert.assertEquals(0, coalMine.getAmount(), 0);
        
        Time.getInstance().step(1001);
        
        Assert.assertEquals(GameConstants.COAL_MINE_PRODUCTION_RATE, coalMine.getAmount(), 0);
        
        station.trade(this.wagons);
        
        Assert.assertEquals(GameConstants.COAL_MINE_PRODUCTION_RATE, this.coalWagon.getLoadedAmount(), 0);
        
        Assert.assertEquals(0, this.ironWagon.getLoadedAmount(), 0);
        Assert.assertEquals(0, this.foodWagon.getLoadedAmount(), 0);
        Assert.assertEquals(0, this.passengersWagon.getLoadedAmount(), 0);
        Assert.assertEquals(0, this.mailWagon.getLoadedAmount(), 0);
        Assert.assertEquals(0, this.woodWagon.getLoadedAmount(), 0);
        Assert.assertEquals(0, this.oilWagon.getLoadedAmount(), 0);
        Assert.assertEquals(0, this.rockWagon.getLoadedAmount(), 0);
        
        Assert.assertEquals(0, coalMine.getAmount(), 0);
    }
    
    @Test
    public void testLoadIron() {
        Assert.assertEquals(0, this.ironWagon.getLoadedAmount(), 0);
        
        final Station station = new Station(15, 16, StationSizes.LARGE);
        
        final IronMine ironMine = new IronMine(13, 14);
        
        station.addProductionSites(ironMine);
        
        Assert.assertEquals(0, ironMine.getAmount(), 0);
        
        Time.getInstance().step(1001);
        
        Assert.assertEquals(GameConstants.IRON_MINE_PRODUCTION_RATE, ironMine.getAmount(), 0);
        
        station.trade(this.wagons);
        
        Assert.assertEquals(GameConstants.IRON_MINE_PRODUCTION_RATE, this.ironWagon.getLoadedAmount(), 10);
        
        Assert.assertEquals(0, this.foodWagon.getLoadedAmount(), 0);
        Assert.assertEquals(0, this.passengersWagon.getLoadedAmount(), 0);
        Assert.assertEquals(0, this.coalWagon.getLoadedAmount(), 0);
        Assert.assertEquals(0, this.mailWagon.getLoadedAmount(), 0);
        Assert.assertEquals(0, this.woodWagon.getLoadedAmount(), 0);
        Assert.assertEquals(0, this.oilWagon.getLoadedAmount(), 0);
        Assert.assertEquals(0, this.rockWagon.getLoadedAmount(), 0);
        Assert.assertEquals(0, this.foodWagon.getLoadedAmount(), 0);
        
        Assert.assertEquals(0, ironMine.getAmount(), 0);
    }
    
    @Test
    public void testLoadOil() {
        Assert.assertEquals(0, this.oilWagon.getLoadedAmount(), 0);
        
        
        final Station station = new Station(15, 16, StationSizes.LARGE);
        
        final OilRig oilrig = new OilRig(13, 14);
        
        station.addProductionSites(oilrig);
        
        Assert.assertEquals(0, oilrig.getAmount(), 0);
        
        Time.getInstance().step(1001);
        
        Assert.assertEquals(GameConstants.OIL_RIG_PRODUCTION_RATE, oilrig.getAmount(), 0);
        
        station.trade(this.wagons);
        
        Assert.assertEquals(GameConstants.OIL_RIG_PRODUCTION_RATE, this.oilWagon.getLoadedAmount(), 0);
        
        Assert.assertEquals(0, this.ironWagon.getLoadedAmount(), 0);
        Assert.assertEquals(0, this.foodWagon.getLoadedAmount(), 0);
        Assert.assertEquals(0, this.passengersWagon.getLoadedAmount(), 0);
        Assert.assertEquals(0, this.coalWagon.getLoadedAmount(), 0);
        Assert.assertEquals(0, this.mailWagon.getLoadedAmount(), 0);
        Assert.assertEquals(0, this.woodWagon.getLoadedAmount(), 0);
        Assert.assertEquals(0, this.rockWagon.getLoadedAmount(), 0);
        Assert.assertEquals(0, this.foodWagon.getLoadedAmount(), 0);
        
        Assert.assertEquals(0, oilrig.getAmount(), 0);
    }
    
    @Test
    public void testLoadRock() {
        Assert.assertEquals(0, this.rockWagon.getLoadedAmount(), 0);
        
        final Station station = new Station(15, 16, StationSizes.LARGE);
        
        final RockCave rockCave = new RockCave(13, 14);
        
        station.addProductionSites(rockCave);
        
        Assert.assertEquals(0, rockCave.getAmount(), 0);
        
        Time.getInstance().step(1001);
        
        Assert.assertEquals(GameConstants.ROCK_CAVE_PRODUCTION_RATE, rockCave.getAmount(), 0);
        
        station.trade(this.wagons);
        
        Assert.assertEquals(GameConstants.ROCK_CAVE_PRODUCTION_RATE, this.rockWagon.getLoadedAmount(), 0);
        
        Assert.assertEquals(0, this.ironWagon.getLoadedAmount(), 0);
        Assert.assertEquals(0, this.foodWagon.getLoadedAmount(), 0);
        Assert.assertEquals(0, this.passengersWagon.getLoadedAmount(), 0);
        Assert.assertEquals(0, this.coalWagon.getLoadedAmount(), 0);
        Assert.assertEquals(0, this.mailWagon.getLoadedAmount(), 0);
        Assert.assertEquals(0, this.woodWagon.getLoadedAmount(), 0);
        Assert.assertEquals(0, this.oilWagon.getLoadedAmount(), 0);
        Assert.assertEquals(0, this.foodWagon.getLoadedAmount(), 0);
        
        Assert.assertEquals(0, rockCave.getAmount(), 0);
    }
    
}
