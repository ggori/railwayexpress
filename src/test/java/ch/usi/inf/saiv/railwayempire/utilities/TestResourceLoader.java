package ch.usi.inf.saiv.railwayempire.utilities;

import org.junit.Assert;
import org.junit.Test;
import org.newdawn.slick.Image;

public class TestResourceLoader {
    
    @Test
    public void testResources() {
        final ResourcesLoader resourcesLoader = ResourcesLoader.getInstance();
        resourcesLoader.loadResources(true);
        Assert.assertNotNull(resourcesLoader);
        
        final Image image = resourcesLoader.getImage("MAIN_MENU_BACKGROUND");
        Assert.assertNotNull(image);
    }
}
