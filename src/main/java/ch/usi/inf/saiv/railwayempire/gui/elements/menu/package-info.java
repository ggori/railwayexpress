/**
 * Main menu or sometimes called splash screen. This package provides all necessary functionality for this applications
 * main menu. Also an in game pause menu can be found here, because they use more or less the same classes and provide
 * almost identical functionality and some more.
 *
 * CreditPanel is used for displaying information about the developers of the application and pointing out if any
 * of the used material is coming from another source thus pointing to it.
 *
 * HighScorePanel works as a holder which displays high score when a player has sold his company. It is used in both
 * main menu and after selling company from pause menu.
 *
 * NewGamePanel provides the functionality to start a new game where a player can set name, company name and choose
 * a logo for the company.
 *
 * PauseMenuPanel provides the functionality to access all the menu options in game.
 *
 * SaveGameInfo is a part for SaveGamePanel which takes care of displaying information about the saved game.
 *
 * SaveGamePanel is a place holder for SaveGameInfo. It displays all the saved games currently found on hard disk drive.
 *
 * SaveLoadPanel provides the functionality of showing both save and load options.
 *
 * SellCompanyPopup ensures to ask the player if he really wants to sell the company. This way it prevents selling
 * company accidentally.
 *
 */
package ch.usi.inf.saiv.railwayempire.gui.elements.menu;