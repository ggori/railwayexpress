package ch.usi.inf.saiv.railwayempire.controllers;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.state.StateBasedGame;

import ch.usi.inf.saiv.railwayempire.gui.GameState;
import ch.usi.inf.saiv.railwayempire.gui.elements.menu.SaveLoadPanel;
import ch.usi.inf.saiv.railwayempire.gui.states.GameplayState;
import ch.usi.inf.saiv.railwayempire.utilities.Time;
import ch.usi.inf.saiv.railwayempire.utilities.serialization.Save;


/**
 * Gameplay controoler.
 */
public final class GameplayController extends AbstractGameController {

    /**
     * The instance of the game.
     */
    private final StateBasedGame game;

    private final GameplayState gameplayState;

    private final SaveLoadPanel savePanel;

    private final SaveLoadPanel loadPanel;


    /**
     * Creates a controller for the gameplay state.
     * 
     * @param game
     *            Instance of the game to initialize.
     * @param container some container
     * @param gameplayState some state
     */
    public GameplayController(final StateBasedGame game, final GameContainer container,
            final GameplayState gameplayState) {
        super(container, game);
        this.game = game;
        this.savePanel = new SaveLoadPanel(new Rectangle(0, 0, container.getWidth(), container.getHeight()), this, 2,
                container, true);
        this.loadPanel = new SaveLoadPanel(new Rectangle(0, 0, container.getWidth(), container.getHeight()), this, 2,
                container, false);
        this.gameplayState = gameplayState;

    }


    /**
     * Toggles the pause screen.
     */
    public void togglePauseScreen() {
        Time.getInstance().togglePause();
        Save.getInstance().setNeedToSaveAgain(true);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setMainPanel() {
        this.gameplayState.setAdditionalPanel(null);
    }

    /**
     * Show the save panel.
     */
    public void setSavePanel() {
        this.savePanel.refresh();
        this.savePanel.getFileName().setFocus(true);
        this.gameplayState.setAdditionalPanel(this.savePanel);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setLoadPanel() {
        this.loadPanel.refresh();
        this.gameplayState.setAdditionalPanel(this.loadPanel);
    }

    /**
     * End the game and goes to the endgame state.
     */
    public void endGame() {
        this.setMainPanel();
        this.togglePauseScreen();
        this.game.enterState(GameState.ENDGAME.ordinal());
    }
}
