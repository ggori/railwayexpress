package ch.usi.inf.saiv.railwayempire.gui.elements;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.gui.GUIContext;

/**
 * Represents a Gui component.
 */
public interface IGuiComponent {
    
    /**
     * Render the element.
     * 
     * @param container
     *            something from slick2d.
     * @param graphics
     *            graphics.
     */
    void render(final GUIContext container, final Graphics graphics);
    
    /**
     * Sets the location of the element.
     * 
     * @param newPosX
     *            x coordinate.
     * @param newPosY
     *            y coordinate.
     */
    void setLocation(final int newPosX, final int newPosY);
    
    /**
     * Set x coordinate.
     * 
     * @param newX
     *            new x coordinate.
     */
    void setX(final int newX);
    
    /**
     * Set y coordinate.
     * 
     * @param newY
     *            new y coordinate.
     */
    void setY(final int newY);
    
    /**
     * Set the center x coordinate of the component.
     * 
     * @param newX
     *            new center x coordinate.
     */
    void setCenterX(final float newX);
    
    /**
     * Set the center y coordinate of the component.
     * 
     * @param newY
     *            new center y coordinate.
     */
    void setCenterY(final float newY);
    
    /**
     * Get the x coordinate of the top left corner.
     * 
     * @return x coordinate.
     */
    int getX();
    
    /**
     * Get the x coordinate of the top left corner.
     * 
     * @return x coordinate.
     */
    int getY();
    
    /**
     * Get the width.
     * 
     * @return width.
     */
    int getWidth();
    
    /**
     * Get the height.
     * 
     * @return height.
     */
    int getHeight();
    
    /**
     * Checks if given coordinates are inside this component.
     * 
     * @param coordX
     *            x coordinate.
     * @param coordY
     *            y coordinate.
     * @return true if given coordinates are inside the component.
     */
    boolean contains(final int coordX, final int coordY);
    
    /**
     * Method called on mouse clicked.
     * 
     * @param button
     *            button.
     * @param coordX
     *            x coordinate.
     * @param coordY
     *            y coordinate.
     * @param clickCount
     *            click count.
     */
    void mouseClicked(final int button, final int coordX, final int coordY, final int clickCount);
    
    /**
     * Method called on mouse pressed.
     * 
     * @param button
     *            button.
     * @param coordX
     *            x coordinate.
     * @param coordY
     *            y coordinate.
     */
    void mousePressed(final int button, final int coordX, final int coordY);
    
    /**
     * Method called on mouse released.
     * 
     * @param button
     *            button.
     * @param coordX
     *            x coordinate.
     * @param coordY
     *            y coordinate.
     */
    void mouseReleased(final int button, final int coordX, final int coordY);
    
    /**
     * Method called on mouse dragged.
     * 
     * @param oldX
     *            old x.
     * @param oldY
     *            old y.
     * @param newX
     *            new x.
     * @param newY
     *            new y.
     */
    void mouseDragged(final int oldX, final int oldY, final int newX, final int newY);
    
    /**
     * Method called on mouse moved.
     * 
     * @param oldX
     *            old x.
     * @param oldY
     *            old y.
     * @param newX
     *            new x.
     * @param newY
     *            new y.
     */
    void mouseMoved(final int oldX, final int oldY, final int newX, final int newY);
    
    /**
     * Method called on mouse wheel moved.
     * 
     * @param charge
     *            charge.
     * @param coordX
     *            x coordinate.
     * @param coordY
     *            y coordinate.
     */
    void mouseWheelMoved(final int charge, int coordX, int coordY);
    
    /**
     * Method called on key pressed.
     * 
     * @param key
     *            key.
     * @param character
     *            character.
     */
    void keyPressed(final int key, final char character);
}
