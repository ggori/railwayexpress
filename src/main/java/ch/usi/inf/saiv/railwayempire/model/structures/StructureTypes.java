package ch.usi.inf.saiv.railwayempire.model.structures;

/** Enum for the structures. */
public enum StructureTypes {
    /** Track. */
    TRACK,

    /** Forest. */
    FOREST,

    /** Station. */
    STATION,

    /** Building. */
    BUILDING,

    /** Coal Mine. */
    COAL_MINE,

    /** Post office. */
    POST_OFFICE,

    /** Rock Cave. */
    ROCK_CAVE,

    /** Saw Mill. */
    SAW_MILL,

    /** Farm. */
    FARM,

    /** Iron Mine. */
    IRON_MINE,

    /** Drill rig. */
    OIL_RIG,

    /** Food factory. */
    FOOD_FACTORY,
}
