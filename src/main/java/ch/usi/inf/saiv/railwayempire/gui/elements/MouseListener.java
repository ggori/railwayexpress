package ch.usi.inf.saiv.railwayempire.gui.elements;

/**
 * Mouse listener.
 */
public interface MouseListener {
    
    /**
     * Notify.
     * 
     * @param button
     *            The button that has been clicked.
     */
    void actionPerformed(Button button);
}
