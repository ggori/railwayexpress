package ch.usi.inf.saiv.railwayempire.gui.elements.bottom;

import org.newdawn.slick.geom.Rectangle;

import ch.usi.inf.saiv.railwayempire.gui.elements.Button;
import ch.usi.inf.saiv.railwayempire.gui.elements.MouseListener;
import ch.usi.inf.saiv.railwayempire.gui.modes.TrackCreationMode;
import ch.usi.inf.saiv.railwayempire.utilities.ResourcesLoader;

/**
 * A panel for the track utilities.
 */
public final class TrackCreationUtilityPanel extends AbstractWorldInfoUtilityPanel {

    /**
     * The width of a button.
     */
    private static final int BUTTON_WIDTH = ResourcesLoader.getInstance().getImage("ARROW_W_BUTTON").getWidth();
    /**
     * The height of a button.
     */
    private static final int BUTTON_HEIGHT = ResourcesLoader.getInstance().getImage("ARROW_W_BUTTON").getHeight();
    /**
     * Margin between buttons
     */
    private static final int BUTTON_MARGIN = 10;

    /**
     * Create the panel.
     * 
     * @param rectangle
     *            rectangle.
     * @param trackCreationMode
     *            track creation mode.
     */
    public TrackCreationUtilityPanel(final Rectangle rectangle, final TrackCreationMode trackCreationMode) {
        super(rectangle);

        final Button westButton = new Button(
                ResourcesLoader.getInstance().getImage("ARROW_W_BUTTON"),
                ResourcesLoader.getInstance().getImage("ARROW_W_BUTTON_HOVER"),
                ResourcesLoader.getInstance().getImage("ARROW_W_BUTTON_PRESSED"),
                this.getX(), this.getY());
        westButton.setListener(new MouseListener() {
            @Override
            public void actionPerformed(final Button button) {
                trackCreationMode.moveLeft();
            }
        });
        this.add(westButton);

        final Button southButton = new Button(
                ResourcesLoader.getInstance().getImage("ARROW_S_BUTTON"),
                ResourcesLoader.getInstance().getImage("ARROW_S_BUTTON_HOVER"),
                ResourcesLoader.getInstance().getImage("ARROW_S_BUTTON_PRESSED"),
                this.getX(), this.getY() + BUTTON_HEIGHT + BUTTON_MARGIN);
        southButton.setListener(new MouseListener() {
            @Override
            public void actionPerformed(final Button button) {
                trackCreationMode.moveDown();
            }
        });
        this.add(southButton);

        final Button nordButton = new Button(
                ResourcesLoader.getInstance().getImage("ARROW_N_BUTTON"),
                ResourcesLoader.getInstance().getImage("ARROW_N_BUTTON_HOVER"),
                ResourcesLoader.getInstance().getImage("ARROW_N_BUTTON_PRESSED"),
                this.getX() + BUTTON_WIDTH + BUTTON_MARGIN, this.getY());
        nordButton.setListener(new MouseListener() {
            @Override
            public void actionPerformed(final Button button) {
                trackCreationMode.moveUp();
            }
        });
        this.add(nordButton);

        final Button eastButton = new Button(ResourcesLoader.getInstance().getImage("ARROW_E_BUTTON"),
                ResourcesLoader.getInstance().getImage("ARROW_E_BUTTON_HOVER"),
                ResourcesLoader.getInstance().getImage("ARROW_E_BUTTON_PRESSED"),
                this.getX() + BUTTON_WIDTH + BUTTON_MARGIN, this.getY() + BUTTON_HEIGHT + BUTTON_MARGIN);
        eastButton.setListener(new MouseListener() {
            @Override
            public void actionPerformed(final Button button) {
                trackCreationMode.moveRight();
            }
        });
        this.add(eastButton);

        final Button createButton = new Button(ResourcesLoader.getInstance().getImage("PLUS_BUTTON"),
                ResourcesLoader.getInstance().getImage("PLUS_BUTTON_HOVER"),
                ResourcesLoader.getInstance().getImage("PLUS_BUTTON_PRESSED"),
                this.getX() + 2 * (BUTTON_WIDTH + BUTTON_MARGIN), this.getY());
        createButton.setListener(new MouseListener() {
            @Override
            public void actionPerformed(final Button button) {
                trackCreationMode.create();
            }
        });
        this.add(createButton);
        
        final Button destroyButton = new Button(ResourcesLoader.getInstance().getImage("REMOVE_TRACK_BUTTON"),
                ResourcesLoader.getInstance().getImage("REMOVE_TRACK_BUTTON_HOVER"),
                ResourcesLoader.getInstance().getImage("REMOVE_TRACK_BUTTON_PRESSED"),
                this.getX() + 2 * (BUTTON_WIDTH + BUTTON_MARGIN), this.getY() + BUTTON_HEIGHT + BUTTON_MARGIN);
        destroyButton.setListener(new MouseListener() {
            @Override
            public void actionPerformed(final Button button) {
                trackCreationMode.destroy();
            }
        });
        this.add(destroyButton);
    }
}
