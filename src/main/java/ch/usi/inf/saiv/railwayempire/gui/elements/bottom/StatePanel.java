package ch.usi.inf.saiv.railwayempire.gui.elements.bottom;

import java.text.NumberFormat;
import java.util.Locale;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.gui.GUIContext;

import ch.usi.inf.saiv.railwayempire.model.Game;
import ch.usi.inf.saiv.railwayempire.gui.elements.AbstractPanel;
import ch.usi.inf.saiv.railwayempire.gui.elements.Label;
import ch.usi.inf.saiv.railwayempire.utilities.GraphicsUtils;
import ch.usi.inf.saiv.railwayempire.utilities.ResourcesLoader;
import ch.usi.inf.saiv.railwayempire.utilities.Time;

/**
 * A panel with informations about the state of the game and the player.
 */
public class StatePanel extends AbstractPanel {
    /**
     * Label width.
     */
    private final static int LABEL_WIDTH = 180;
    /**
     * Label height.
     */
    private final static int LABEL_HEIGHT = 20;
    /**
     * The margin for the logo.
     */
    private final static int IMAGE_MARGIN = 8;
    /**
     * Margin.
     */
    private final static int MARGIN = 5;
    /**
     * The image of the logo.
     */
    private final Image logo;

    /**
     * Create the panel.
     * 
     * @param newX
     *            coordinate x
     * @param newY
     *            coordinate y
     * @param newWidth
     *            width of the panel
     * @param newHeight
     *            height of the panel
     */
    public StatePanel(final int newX, final int newY, final int newWidth, final int newHeight) {
        super(new Rectangle(newX, newY, newWidth, newHeight));
        this.logo = ResourcesLoader.getInstance().getImage(Game.getInstance().getPlayer().getLogo());

        this.add(new Label(this.getX() + StatePanel.MARGIN + this.logo.getWidth() + 2 * IMAGE_MARGIN,
                this.getY() + StatePanel.MARGIN + StatePanel.LABEL_HEIGHT * 0,
                StatePanel.LABEL_WIDTH,
                StatePanel.LABEL_HEIGHT,
                null) {
            @Override
            public void render(final GUIContext container, final Graphics graphics) {
                final String playerName = Game.getInstance().getPlayer().getPlayerName();
                GraphicsUtils.drawShadowText(graphics, playerName, this.getX(), this.getY());
            }
        });

        this.add(new Label(this.getX() + StatePanel.MARGIN + this.logo.getWidth() + 2 * IMAGE_MARGIN,
                this.getY() + StatePanel.MARGIN + StatePanel.LABEL_HEIGHT * 1,
                StatePanel.LABEL_WIDTH,
                StatePanel.LABEL_HEIGHT,
                null) {
            @Override
            public void render(final GUIContext container, final Graphics graphics) {
                final String companyName = Game.getInstance().getPlayer().getCompanyName();
                GraphicsUtils.drawShadowText(graphics, companyName, this.getX(), this.getY());
            }
        });

        this.add(new Label(this.getX() + StatePanel.MARGIN + this.logo.getWidth() + 2 * IMAGE_MARGIN,
                this.getY() + StatePanel.MARGIN + StatePanel.LABEL_HEIGHT * 2,
                StatePanel.LABEL_WIDTH,
                StatePanel.LABEL_HEIGHT,
                null) {
            @Override
            public void render(final GUIContext container, final Graphics graphics) {
                GraphicsUtils.drawShadowText(graphics, Time.getInstance().toInfoDate(), this.getX(), this.getY());
            }
        });

        this.add(new Label(this.getX() + StatePanel.MARGIN + this.logo.getWidth() + 2 * IMAGE_MARGIN,
                this.getY() + StatePanel.MARGIN + StatePanel.LABEL_HEIGHT * 3,
                StatePanel.LABEL_WIDTH,
                StatePanel.LABEL_HEIGHT,
                null) {
            @Override
            public void render(final GUIContext container, final Graphics graphics) {
                final NumberFormat formatter = NumberFormat.getCurrencyInstance(Locale.US);
                final String capital = formatter.format(Game.getInstance().getPlayer().getAvailableMoney());
                GraphicsUtils.drawShadowText(graphics, capital, this.getX(), this.getY());
            }
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void render(final GUIContext container, final Graphics graphics) {
        super.render(container, graphics);
        graphics.drawImage(this.logo, this.getX() + MARGIN + IMAGE_MARGIN, this.getY() + this.getHeight() / 2
                - this.logo.getHeight() / 2);

    }
}
