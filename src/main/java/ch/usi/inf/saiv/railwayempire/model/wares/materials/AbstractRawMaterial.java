package ch.usi.inf.saiv.railwayempire.model.wares.materials;


import ch.usi.inf.saiv.railwayempire.model.wares.AbstractWare;


/**
 * Abstract class representing raw extracted goods to be transported between cities.
 */
public abstract class AbstractRawMaterial extends AbstractWare {

    /**
     * serialVersionUID generated by eclipse.
     */
    private static final long serialVersionUID = -372463489316682901L;

    /**
     * Constructor.
     *
     * @param newQuantity
     *         new quantity.
     */
    public AbstractRawMaterial(final double newQuantity) {
        super(newQuantity);
    }
}
