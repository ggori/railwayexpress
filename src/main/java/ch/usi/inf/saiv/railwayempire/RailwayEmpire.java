package ch.usi.inf.saiv.railwayempire;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

import ch.usi.inf.saiv.railwayempire.gui.states.EndgameState;
import ch.usi.inf.saiv.railwayempire.gui.states.GameplayState;
import ch.usi.inf.saiv.railwayempire.gui.states.MainMenuState;
import ch.usi.inf.saiv.railwayempire.utilities.SystemConstants;



/** Main class used to provide an entry point for the application. */
public final class RailwayEmpire extends StateBasedGame {
    /** Edit if want to change resolution. */
    private static final int DEFAULT_WIDTH = 1680;
    /** Edit if want to change resolution. */
    private static final int DEFAULT_HEIGHT = 945;
    /** Edit if want to change fullscreen mode. */
    private static final boolean DEFAULT_ISFULLSCREEN = false;

    /**
     * Entry point for the application.
     * 
     * @param args
     *            Command line arguments.
     * 
     * @throws SlickException
     *             Thrown when something bad happens during slick methods such as render() or update().
     */
    public static void main(final String[] args) throws SlickException {
        final int width;
        final int height;
        final boolean fullScreen;
        if (args.length == SystemConstants.THREE) {
            width = Integer.parseInt(args[0]);
            height = Integer.parseInt(args[1]);
            fullScreen = "true".equals(args[2]) || "True".equals(args[2]);
        } else {
            width = RailwayEmpire.DEFAULT_WIDTH;
            height = RailwayEmpire.DEFAULT_HEIGHT;
            fullScreen = RailwayEmpire.DEFAULT_ISFULLSCREEN;
        }

        final AppGameContainer app = new AppGameContainer(new RailwayEmpire(), width, height, fullScreen);

        // Disable fps counter
        app.setShowFPS(false);
        app.start();
    }

    /** Empty private constructor. */
    private RailwayEmpire() {
        super("Railway Empire");
    }

    /**
     * Initializes the list of game states.
     * 
     * @param gameContainer
     *            The container of the game.
     * 
     * @throws SlickException
     *             A Slick 2d exception.
     */
    @Override
    public void initStatesList(final GameContainer gameContainer) throws SlickException {
        this.addState(new MainMenuState());
        this.addState(new GameplayState());
        this.addState(new EndgameState());
    }
}
