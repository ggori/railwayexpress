package ch.usi.inf.saiv.railwayempire.model.trains;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import ch.usi.inf.saiv.railwayempire.model.structures.Station;

/**
 * Class used to store the Route of a Train. It will contain a list of station to stop and what to do.
 */
public final class Route implements Serializable {
    
    /**
     * serialVersionUID generated by eclipse.
     */
    private static final long serialVersionUID = 8857507783568291319L;
    /**
     * A list containing the stations of the route.
     */
    private final List<Station> stations;
    /**
     * A list containing the stops of the route.
     */
    private final List<Stop> stops;
    /**
     * The current position of the train in the route.
     */
    private int currentPosition;
    
    /**
     * Create a new route.
     */
    public Route() {
        this.stations = new ArrayList<Station>();
        this.stops = new ArrayList<Stop>();
        this.currentPosition = 0;
    }
    
    /**
     * Get the list of stations.
     * 
     * @return list of stations.
     */
    public List<Station> getPath() {
        return this.stations;
    }
    
    /**
     * Get the path of the route.
     * 
     * @return the path of the route
     */
    public List<Stop> getStops() {
        return this.stops;
    }
    
    /**
     * Get the current station.
     * 
     * @return The station at which the train is.
     */
    public Station getCurrentStation() {
        if (this.hasNoStops()) {
            return null;
        } else {
            return this.stations.get(this.currentPosition);
        }
    }
    
    /**
     * Get the current stop.
     * 
     * @return The stop at which the train is.
     */
    public Stop getCurrentStop() {
        return this.stops.get(this.currentPosition);
    }
    
    /**
     * Get the next destination of the train and update the queue.
     * 
     * @return The next destination.
     */
    public Station getNextDestination() {
        ++this.currentPosition;
        if (this.currentPosition == this.stations.size() - 1 && this.stations.get(this.currentPosition)
                .equals(this.stations.get(0))) {
            ++this.currentPosition;
        }
        if (this.currentPosition == this.stations.size()) {
            this.currentPosition = 0;
        }
        return this.stations.get(this.currentPosition);
    }
    
    /**
     * Add a new stop to the path.
     * 
     * @param station
     *            Station of the new stop.
     * @param position
     *            the position in the route.
     */
    public void addStop(final Station station, final int position) {
        this.stations.add(position, station);
        this.stops.add(position, new Stop());
    }
    
    /**
     * Add a new station at the end of the route.
     * 
     * @param station
     *            The station to add.
     * 
     * @throws IllegalArgumentException
     *             if the station to add is the same as the last station in the
     *             route.
     */
    public void addStop(final Station station) {
        if (!this.hasNoStops() && this.getLastStation() == station) {
            throw new IllegalArgumentException("A rout cannot have the same station as two consequent stops!");
        }
        this.stations.add(station);
        this.stops.add(new Stop());
    }
    
    /**
     * Get the last station in the route.
     * 
     * @return the last station in the route, or <code>null</code> if the route is empty.
     */
    public Station getLastStation() {
        return this.hasNoStops() ? null : this.stations.get(this.stations.size() - 1);
    }
    
    /**
     * Remove a stop from the path.
     * 
     * @param stationIndex
     *            The station of the stop that has to be removed.
     */
    public void removeStop(final int stationIndex) {
        this.stations.remove(stationIndex);
        this.stops.remove(stationIndex);
        
    }
    
    /**
     * Indcates if the route is empty or if it has stops.
     * 
     * @return <code>false</code> if the route has at least one stop. <code>true</code> true otherwise.
     */
    public boolean hasNoStops() {
        return this.getStops().isEmpty();
    }
    
    /**
     * Returns the first station of the route.
     * 
     * @return The first station of the route or <code>null</code> if the route is empty.
     */
    public Station getFirstStation() {
        return this.stations.isEmpty() ? null : this.stations.get(0);
    }
}
