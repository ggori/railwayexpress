package ch.usi.inf.saiv.railwayempire.model.wares.materials;


import ch.usi.inf.saiv.railwayempire.model.wares.WareTypes;


/**
 * Class representing a pile of Wood.
 */
public class Wood extends AbstractRawMaterial {

    /**
     * The value of one unit of wood.
     */
    private static final int VALUE = 15;
    /**
     * serialVersionUID generated by eclipse.
     */
    private static final long serialVersionUID = -8070308276451643436L;
    /**
     * The name of the contained ware.
     */
    private static final String WARE_NAME = "Logs";

    /**
     * Constructor.
     *
     * @param newQuantity
     *         new quantity.
     */
    public Wood(final double newQuantity) {
        super(newQuantity);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public WareTypes getType() {
        return WareTypes.WOOD;
    }

    /**
     * {@inheritDoc}
     */
    public String getTypeName() {
        return WARE_NAME;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getValue() {
        return VALUE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return (int) this.getQuantity() + " " + WARE_NAME;
    }
}
