package ch.usi.inf.saiv.railwayempire.gui.elements.bottom;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.gui.GUIContext;

import ch.usi.inf.saiv.railwayempire.controllers.CenterPanelsController;
import ch.usi.inf.saiv.railwayempire.controllers.GameModesController;
import ch.usi.inf.saiv.railwayempire.gui.elements.AbstractPanel;
import ch.usi.inf.saiv.railwayempire.gui.elements.Button;
import ch.usi.inf.saiv.railwayempire.gui.elements.IGuiComponent;
import ch.usi.inf.saiv.railwayempire.gui.elements.MouseListener;
import ch.usi.inf.saiv.railwayempire.gui.viewers.CoordinatesManager;
import ch.usi.inf.saiv.railwayempire.utilities.ResourcesLoader;

/**
 * The bottom pane during the game is used to display information regarding the currently selected element.
 */
public final class BottomPanel extends AbstractPanel {
    /**
     * Minimap offset.
     */
    private final static int MINIMAP_OFFSET = 10;
    /**
     * Vertical distance between button panels.
     */
    private final static int BUTTONS_MARGIN = 5;
    /**
     * Distance from zoom buttons to the bottom (and double of that is the distance to the side.. trust me).
     */
    private final static int ZOOM_BUTTONS_MARGIN = 10;
    /**
     * Current info panel.
     */
    private IGuiComponent infoPanel;
    
    /**
     * Constructor.
     * 
     * @param rectangle
     *            shape of component.
     * @param modesController
     *            modes controller.
     * @param centerPanelsController
     *            center panels controller.
     */
    public BottomPanel(final Rectangle rectangle,
        final GameModesController modesController,
        final CenterPanelsController centerPanelsController) {
        super(rectangle);
        
        final StatesButtonsPanel buttonsPanel = new StatesButtonsPanel(this.getX(), this.getY(), centerPanelsController);
        
        final TimeButtonsPanel timePanel = new TimeButtonsPanel(
            this.getX(),
            this.getY() + buttonsPanel.getHeight() + BottomPanel.BUTTONS_MARGIN);
        
        final StatePanel statePanel = new StatePanel(
            this.getX(),
            this.getY() + buttonsPanel.getHeight() + timePanel.getHeight() + BottomPanel.BUTTONS_MARGIN,
            buttonsPanel.getWidth(), this.getHeight()
            - buttonsPanel.getHeight() - timePanel.getHeight());
        
        final Minimap minimap = new Minimap(
            this.getWidth() - this.getHeight() * 2 - BottomPanel.MINIMAP_OFFSET,
            this.getY() - BottomPanel.MINIMAP_OFFSET,
            (this.getHeight() + BottomPanel.MINIMAP_OFFSET * 2) * 2,
            this.getHeight() + BottomPanel.MINIMAP_OFFSET * 2);
        
        final ResourcesLoader loader = ResourcesLoader.getInstance();
        final int zoomButtonSize = loader.getImage("MINIMAP_PLUS_BUTTON").getWidth();
        final Button zoomInButton = new Button(loader.getImage("MINIMAP_PLUS_BUTTON"),
            loader.getImage("MINIMAP_PLUS_BUTTON_HOVER"),
            loader.getImage("MINIMAP_PLUS_BUTTON_PRESSED"),
            this.getWidth() - this.getHeight() * 2 + BottomPanel.ZOOM_BUTTONS_MARGIN * 4,
            // I honestly don't know why its * 4 instead of * 2 in the previous line (^ here). But seems to work.
            this.getY() + this.getHeight() - zoomButtonSize - BottomPanel.ZOOM_BUTTONS_MARGIN);
        zoomInButton.setListener(new MouseListener() {
            @Override
            public void actionPerformed(final Button button) {
                for (int i = 0; i < 10; ++i) {
                    // I don't care. \_/
                    CoordinatesManager.getInstance().zoomIn();
                }
            }
        });
        
        final Button zoomOutButton = new Button(loader.getImage("MINIMAP_MINUS_BUTTON"),
            loader.getImage("MINIMAP_MINUS_BUTTON_HOVER"),
            loader.getImage("MINIMAP_MINUS_BUTTON_PRESSED"),
            this.getWidth() - zoomButtonSize - 2 * BottomPanel.ZOOM_BUTTONS_MARGIN,
            this.getY() + this.getHeight() - zoomButtonSize - BottomPanel.ZOOM_BUTTONS_MARGIN);
        zoomOutButton.setListener(new MouseListener() {
            @Override
            public void actionPerformed(final Button button) {
                for (int i = 0; i < 10; ++i) {
                    // c(_) <--> Care cup, still empty.
                    CoordinatesManager.getInstance().zoomOut();
                }
            }
        });
        
        final int newCentralPanelWidth = this.getWidth() - buttonsPanel.getWidth() - minimap.getWidth();
        
        centerPanelsController.initBottom(
            new Rectangle(this.getX() + buttonsPanel.getWidth(), this.getY(),
                newCentralPanelWidth, this.getHeight()),
                this);
        
        this.add(buttonsPanel);
        this.add(timePanel);
        this.add(statePanel);
        this.add(minimap);
        this.add(zoomInButton);
        this.add(zoomOutButton);
        
        this.setBackground(ResourcesLoader.getInstance().getImage("BACKGROUND"));
    }
    
    /**
     * Get current info panel.
     * 
     * @return info panel.
     */
    public IGuiComponent getInfoPanel() {
        return this.infoPanel;
    }
    
    /**
     * Set current info panel.
     * 
     * @param newInfoPanel
     *            the infoPanel.
     */
    public void setInfoPanel(final AbstractInfoPanel newInfoPanel) {
        this.infoPanel = newInfoPanel;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void render(final GUIContext container, final Graphics graphics) {
        super.render(container, graphics);
        this.infoPanel.render(container, graphics);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void mouseClicked(final int button, final int coordX, final int coordY, final int clickCount) {
        super.mouseClicked(button, coordX, coordY, clickCount);
        if (this.infoPanel.contains(coordX, coordY)) {
            this.infoPanel.mouseClicked(button, coordX, coordY, clickCount);
        }
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void mousePressed(final int button, final int coordX, final int coordY) {
        super.mousePressed(button, coordX, coordY);
        if (this.infoPanel.contains(coordX, coordY)) {
            this.infoPanel.mousePressed(button, coordX, coordY);
        }
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void mouseReleased(final int button, final int coordX, final int coordY) {
        super.mouseReleased(button, coordX, coordY);
        if (this.infoPanel.contains(coordX, coordY)) {
            this.infoPanel.mouseReleased(button, coordX, coordY);
        }
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void mouseDragged(final int oldX, final int oldY, final int newX, final int newY) {
        super.mouseDragged(oldX, oldY, newX, newY);
        if (this.infoPanel.contains(oldX, oldY) || this.infoPanel.contains(newX, newY)) {
            this.infoPanel.mouseDragged(oldX, oldY, newX, newY);
        }
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void mouseMoved(final int oldX, final int oldY, final int newX, final int newY) {
        super.mouseMoved(oldX, oldY, newX, newY);
        if (this.infoPanel.contains(oldX, oldY) || this.infoPanel.contains(newX, newY)) {
            this.infoPanel.mouseMoved(oldX, oldY, newX, newY);
        }
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void mouseWheelMoved(final int charge, final int coordX, final int coordY) {
        super.mouseWheelMoved(charge, coordX, coordY);
        if (this.infoPanel.contains(coordX, coordY)) {
            this.infoPanel.mouseWheelMoved(charge, coordX, coordY);
        }
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void keyPressed(final int key, final char character) {
        super.keyPressed(key, character);
        this.infoPanel.keyPressed(key, character);
    }
}
