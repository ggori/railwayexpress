package ch.usi.inf.saiv.railwayempire.utilities;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.newdawn.slick.Color;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.Sound;
import org.newdawn.slick.loading.LoadingList;
import org.newdawn.slick.util.ResourceLoader;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * Taken from the Slick2d wiki.
 * http://slick.cokeandcode.com/wiki/doku.php?id=resource_manager_tutorial
 */
public final class ResourcesLoader {
    
    /**
     * The unique instance of the resource loader.
     */
    private static ResourcesLoader uniqueInstance = new ResourcesLoader();
    /**
     * The map containing the loaded images.
     */
    private final Map<String, Image> imageResources;
    /**
     * The map containing the loaded sounds.
     */
    private final Map<String, Sound> soundResources;
    /**
     * The map containing the loaded colors.
     */
    private final Map<String, Color> colorResources;
    
    /**
     * Creates a new resource loader.
     */
    private ResourcesLoader() {
        this.imageResources = new HashMap<String, Image>();
        this.soundResources = new HashMap<String, Sound>();
        this.colorResources = new HashMap<String, Color>();
    }
    
    /**
     * Returns the unique instance of the resource loader.
     * 
     * @return The unique instance of the resource loader.
     */
    public static ResourcesLoader getInstance() {
        return ResourcesLoader.uniqueInstance;
    }
    
    /**
     * Adds the element from the resource file as an image.
     * 
     * @param element
     *            The element to add.
     * @throws SlickException
     *             An exception.
     */
    private void addElementAsImage(final Element element) throws SlickException {
        this.loadImage(element.getAttribute("id"), element.getTextContent());
    }
    
    /**
     * Adds the element from the resource file as a color.
     * 
     * @param element
     *            The element to add.
     * @throws SlickException
     *             An exception.
     */
    private void addElementAsColor(final Element element) throws SlickException {
        final String colorString = element.getTextContent();
        
        if (element.getTextContent() == null) {
            throw new SlickException("Color " + colorString + " is not a valid color");
        }
        
        final String[] vals = colorString.split(",");
        final int number = Integer.parseInt(element.getAttribute("number"));
        final String id = element.getAttribute("id");
        
        for (int i = 0; i < number; ++i) {
            final Color col = new Color(
                Integer.parseInt(vals[0]),
                Integer.parseInt(vals[1]),
                Integer.parseInt(vals[2])
                );
            this.colorResources.put(id + i, col);
        }
        
        if (number == 0) {
            final Color col = new Color(
                Integer.parseInt(vals[0]),
                Integer.parseInt(vals[1]),
                Integer.parseInt(vals[2])
                );
            this.colorResources.put(id, col);
        }
    }
    
    /**
     * Adds an element from the resource file as a sound.
     * 
     * @param element
     *            The element to add.
     * @throws SlickException
     *             An exception.
     */
    private void addElementAsSound(final Element element) throws SlickException {
        this.loadSound(element.getAttribute("id"), element.getTextContent());
    }
    
    /**
     * Returns an image stored with the given id.
     * 
     * @param id
     *            The id of the image to retrieve.
     * @return The image stored with the received id, null id there is no image stored with that id.
     */
    public Image getImage(final String id) {
        final Image ret = this.imageResources.get(id);
        if (null == ret) {
            throw new NullPointerException("Texture not found. String was: " + id);
        }
        return ret;
    }
    
    /**
     * Returns an color stored with the given id.
     * 
     * @param id
     *            The id of the color to retrieve.
     * @return The color stored with the received id, null id there is no image stored with that id.
     */
    public Color getColor(final String id) {
        return this.colorResources.get(id);
    }
    
    /**
     * Loads an image and stores it in the map of loaded images.
     * 
     * @param id
     *            The id associated with the image.
     * @param path
     *            The path of the image.
     * @throws SlickException
     *             An exception.
     */
    private void loadImage(final String id, final String path) throws SlickException {
        if (path == null || path.length() == 0) {
            throw new SlickException("Image resource [" + id + "] has invalid path");
        }
        
        Image image;
        try {
            image = new Image(path);
        } catch (final SlickException exception) {
            throw new SlickException("Could not load image [" + id + "]", exception);
        }
        
        this.imageResources.put(id, image);
    }
    
    /**
     * Load game resources.
     */
    public void loadResources(final boolean deferred) {
        try {
            this.loadResources(ResourceLoader.getResource("graphics_xml/menu_buttons.xml").openStream(), deferred);
            this.loadResources(ResourceLoader.getResource("graphics_xml/buttons.xml").openStream(), deferred);
            this.loadResources(ResourceLoader.getResource("graphics_xml/resources.xml").openStream(), deferred);
            this.loadResources(ResourceLoader.getResource("graphics_xml/logos.xml").openStream(), deferred);
            this.loadResources(ResourceLoader.getResource("graphics_xml/traincomponents_textures.xml").openStream(),
                deferred);
            this.loadResources(ResourceLoader.getResource("xml/colors.xml").openStream(), deferred);
            this.loadResources(ResourceLoader.getResource("graphics_xml/transition-smooth.xml").openStream(), deferred);
            this.loadResources(ResourceLoader.getResource("graphics_xml/tiles-" + GameConstants.TILES_THEME + ".xml")
                .openStream(), deferred);
        } catch (final IOException exception) {
            Log.exception(exception);
        } catch (final SlickException exception) {
            Log.exception(exception);
        }
    }
    
    /**
     * Loads the resources from the input stream.
     * 
     * @param inputStream
     *            THe input stream from which to load the resources.
     * @param deferred
     *            Whether or not to load the resources in a deferred way.
     * @throws SlickException
     *             An exception.
     */
    public void loadResources(final InputStream inputStream, final boolean deferred) throws SlickException {
        final DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        
        DocumentBuilder documentBuilder = null;
        try {
            documentBuilder = documentBuilderFactory.newDocumentBuilder();
        } catch (final ParserConfigurationException exception) {
            Log.exception(exception);
        }
        
        Document document = null;
        try {
            document = documentBuilder.parse(inputStream);
        } catch (final SAXException exception) {
            exception.printStackTrace();
        } catch (final IOException exception) {
            exception.printStackTrace();
        }
        
        document.getDocumentElement().normalize();
        
        final NodeList resourcesList = document.getElementsByTagName("resource");
        
        LoadingList.setDeferredLoading(deferred);
        
        for (int resourceIndex = 0; resourceIndex < resourcesList.getLength(); ++resourceIndex) {
            final Node resourceNode = resourcesList.item(resourceIndex);
            
            if (resourceNode.getNodeType() == Node.ELEMENT_NODE) {
                final Element resourceElement = (Element) resourceNode;
                
                final String resourceType = resourceElement.getAttribute("type");
                
                if ("image".equals(resourceType)) {
                    this.addElementAsImage(resourceElement);
                } else if ("sound".equals(resourceType)) {
                    this.addElementAsSound(resourceElement);
                } else if ("color".equals(resourceType)) {
                    this.addElementAsColor(resourceElement);
                }
            }
        }
    }
    
    /**
     * Loads a sound and stores it in the map of loaded sounds.
     * 
     * @param id
     *            The id associated with the sound.
     * @param path
     *            The path of the sound.
     * @throws SlickException
     *             An exception.
     *
     */
    private void loadSound(final String id, final String path) throws SlickException {
        if (path == null || path.length() == 0) {
            throw new SlickException("Sound resource [" + id + "] has invalid path");
        }
        
        Sound sound;
        
        try {
            sound = new Sound(path);
        } catch (final SlickException e) {
            throw new SlickException("Could not load sound [" + id + "]", e);
        }
        
        this.soundResources.put(id, sound);
    }
}
