package ch.usi.inf.saiv.railwayempire.gui.elements;

import org.newdawn.slick.geom.Shape;


/**
 * Concrete implementation of AbstractPanel.
 */
public class SimplePanel extends AbstractPanel {
    
    /**
     * Constructor.
     * 
     * @param newShape
     *            shape.
     */
    public SimplePanel(final Shape newShape) {
        super(newShape);
    }
}
