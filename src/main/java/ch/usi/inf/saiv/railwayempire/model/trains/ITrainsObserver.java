package ch.usi.inf.saiv.railwayempire.model.trains;

/**
 * Defines the behavior of the observer for a train.
 * 
 */
public interface ITrainsObserver {
    
    /**
     * Notify the observer that a train has been added to the train manager.
     * 
     * @param train
     *            The train which was added.
     */
    void notifyTrainAdded(Train train);
    
    /**
     * Notify the observer that a train has been removed from the train manager.
     * 
     * @param train
     *            The train which was removed.
     */
    void notifyTrainRemoved(Train train);
    
}
