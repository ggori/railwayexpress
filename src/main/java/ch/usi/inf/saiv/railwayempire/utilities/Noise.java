package ch.usi.inf.saiv.railwayempire.utilities;

import java.util.ArrayList;
import java.util.List;

/**
 * A representation of a random white noise. The noise is first created with a frequency equal to 1 / size. The class
 * provides a method used to generate a fractal noise.
 *
 */
public class Noise {
    
    /** The lower bound for the values in the noise. */
    private static final double NOISE_MIN_VALUE = 0.0;
    /** The upper bound for the values in the noise. */
    private static final double NOISE_MAX_VALUE = 1.0;
    /** The size of the noise. */
    private int size;
    /** The values contained in the noise. */
    private List<Double> values;
    
    /**
     * Creates a new square representation of a noise, filled with random values between 0.0 and 1.0.
     *
     * @param inputSize
     *         The size of the noise to create.
     */
    public Noise(final int inputSize) {
        this.checkNoiseSize(inputSize);
        this.setSize(inputSize);
        this.setValues(new ArrayList<Double>(inputSize * inputSize));
        
        for (int y = 0; y < inputSize; ++y) {
            for (int x = 0; x < inputSize; ++x) {
                if (x == 0 || y == 0 || y == inputSize - 1 || x == inputSize - 1) {
                    this.values.add(0.0);
                } else if (this.getDistance(x, y, inputSize / 2, inputSize / 2) >= 1.02 * inputSize / 2) {
                    this.values.add(0.0);
                } else {
                    this.values.add(Math.random());
                }
            }
        }
    }
    
    /**
     * Return the real distance between two points.
     * 
     * @param posX1
     *        x coordinate of point 1.
     * @param posY1
     *        y coordinate of point 1.
     * @param posX2
     *        x coordinate of point 2.
     * @param posY2
     *        y coordinate of point 2.
     * @return distance.
     */
    private int getDistance(final int posX1, final int posY1, final int posX2, final int posY2) {
        return (int) Math.sqrt(Math.pow(posX1 - posX2, 2) + Math.pow(posY1 - posY2, 2));
    }
    
    /**
     * Returns the size (side) of the noise.
     *
     * @return The size of the noise
     */
    public final int getSize() {
        return this.size;
    }
    
    /**
     * Set the size of the noise.
     *
     * @param newSize
     *         The new size
     */
    private void setSize(final int newSize) {
        this.size = newSize;
    }
    
    /**
     * Set the Values List.
     *
     * @param newValues
     *         The new values
     */
    private void setValues(final List<Double> newValues) {
        this.values = newValues;
    }
    
    /**
     * Returns the value of the noise corresponding to the given coordinates.
     *
     * @param posX
     *         The x coordinate of the value.
     * @param posY
     *         The y coordinate of the value.
     * @return The value in the noise.
     */
    public final double getValue(final int posX, final int posY) {
        this.checkCoordinates(posX, posY, this.getSize());
        return this.values.get((int) this.getLinearCoordinates(posX, posY));
    }
    
    /**
     * Sets the point in the noise corresponding to the coordinates to be equal to the received value.
     *
     * @param posX
     *         The x coordinate of the point to set.
     * @param posY
     *         The y coordinate of the point to set.
     * @param value
     *         The value to assign to the point.
     */
    private void setValue(final int posX, final int posY, final double value) {
        this.checkCoordinates(posX, posY, this.getSize());
        if (value < NOISE_MIN_VALUE || value > NOISE_MAX_VALUE) {
            throw new IllegalArgumentException("Value must be > 0.0 and < 1.0!");
        }
        this.values.set((int) this.getLinearCoordinates(posX, posY), value);
    }
    
    /**
     * Adds the noise with the other given noise. Every resulting value will be* a mix composed by persistence local
     * values + (1 - persistence) * other* values.
     *
     * @param other
     *         The noise to be added with.
     * @param persistence
     *         The persistence of the values of this noise.
     */
    public final void addWith(final Noise other, final double persistence) {
        if (persistence <= 0.0 || persistence >= 1.0) {
            throw new IllegalArgumentException("Persistence must be > 0.0 and < 1.0!");
        }
        if (this.getSize() != other.getSize()) {
            throw new IllegalArgumentException("The two noises must have the same size!");
        }
        
        for (int posX = 0; posX < this.size; ++posX) {
            for (int posY = 0; posY < this.getSize(); ++posY) {
                final double newValue = this.getValue(posX, posY) * persistence + other.getValue(posX, posY)
                        * (1 - persistence);
                this.setValue(posX, posY, newValue);
            }
        }
    }
    
    /**
     * Enlarges the noise and fills the gaps interpolating linearly between the old values.
     *
     * @param newSize
     *         The at which to enlarge the noise.
     */
    public final void enlargeTo(final int newSize) {
        this.checkNoiseSize(newSize);
        
        final int ratio = newSize / this.getSize();
        final List<Double> newValues = new ArrayList<Double>(newSize * newSize);
        for (int i = 0; i < newSize * newSize; ++i) {
            newValues.add(null);
        }
        
        for (int posX = 0; posX < newSize; ++posX) {
            final int xLeft = posX / ratio;
            final int xRight = (xLeft + 1) % this.getSize();
            
            for (int posY = 0; posY < newSize; ++posY) {
                final int yTop = posY / ratio;
                final int yBottom = (yTop + 1) % this.getSize();
                
                if (posX % ratio == SystemConstants.ZERO && posY % ratio == SystemConstants.ZERO) {
                    newValues.set(this.getLinearCoordinates(posX, posY, newSize), this.getValue(xLeft, yTop));
                } else {
                    final double topLeft = this.getValue(xLeft, yTop);
                    final double topRight = this.getValue(xRight, yTop);
                    final double bottomLeft = this.getValue(xLeft, yBottom);
                    final double bottomRight = this.getValue(xRight, yBottom);
                    
                    final double xAlpha = (double)(posX % ratio) / ratio;
                    final double top = this.linearInterpolation(topLeft, topRight, xAlpha);
                    final double bottom = this.linearInterpolation(bottomLeft, bottomRight, xAlpha);
                    
                    final double yAlpha = (double)(posY % ratio) / ratio;
                    final double newValue = this.linearInterpolation(top, bottom, yAlpha);
                    newValues.set(this.getLinearCoordinates(posX, posY, newSize), newValue);
                }
            }
        }
        this.setValues(newValues);
        this.setSize(newSize);
    }
    
    /**
     * Returns the linear interpolation between the two values considering the ratio between the two.
     *
     * @param val1
     *         The first value.
     * @param val2
     *         The second value.
     * @param alpha
     *         The ration between the two values calculated from the first
     *         one.
     * @return The interpolated value.
     */
    private double linearInterpolation(final double val1, final double val2, final double alpha) {
        return val1 + (val2 - val1) * alpha;
    }
    
    /**
     * Converts the bi-dimensional coordinates of a point on the noise into an ndex used to access the internal
     * one-dimensional representation.
     *
     * @param posX
     *         The x coordinate of the point.
     * @param posY
     *         The y coordinate of the point.
     * @return The index to use in the one-dimensional representation.
     */
    private float getLinearCoordinates(final float posX, final float posY) {
        this.checkCoordinates(posX, posY, this.getSize());
        return this.getSize() * posY + posX;
    }
    
    /**
     * Converts the bi-dimensional coordinates of a point on the noise into an index used to access the internal
     * one-dimensional representation, the method considers a noise of the given size.
     *
     * @param posX
     *         The x coordinate of the point.
     * @param posY
     *         The y coordinate of the point.
     * @param noiseSize
     *         The size of the noise to consider for the conversion.
     * @return The index to use in the one-dimensional representation.
     */
    private int getLinearCoordinates(final int posX, final int posY, final int noiseSize) {
        this.checkCoordinates(posX, posY, noiseSize);
        return noiseSize * posY + posX;
    }
    
    /**
     * Generates a fractal noise with the size of the actual one. The fractal noise will be generated by performing
     * a number of iterations equal to the received octaves number. If the size of the new noise to generate during
     * the iterations gets to 2 before the number of octaves is reached the generation is stopped anyways.
     * During the generation the noises will be added using the received persistence value.
     *
     * @param octaves
     *         The number of iterations to perform during the
     *         generation.
     * @param persistence
     *         The persistence to use for addition of noises.
     */
    public final void generateFractalNoise(final int octaves, final double persistence) {
        int newSize = this.getSize() / 2;
        for (int octave = 0; octave < octaves && newSize >= 2; ++octave) {
            final Noise other = new Noise(newSize);
            other.enlargeTo(this.getSize());
            this.addWith(other, persistence);
            newSize /= 2;
        }
    }
    
    /**
     * Checks if the given coordinates are valid in a noise of the received size.
     *
     * @param posX
     *         The x coordinate.
     * @param posY
     *         The y coordinate.
     * @param noiseSize
     *         The size of the noise.
     */
    private void checkCoordinates(final float posX, final float posY, final int noiseSize) {
        if (posX >= noiseSize || posY >= noiseSize) {
            throw new IllegalArgumentException("Coordinates are out of bound!");
        }
        if (posX < SystemConstants.ZERO || posY < SystemConstants.ZERO) {
            throw new IllegalArgumentException("Coordinates cannot be < 0!");
        }
    }
    
    /**
     * Checks if the given size is valid for a noise.
     *
     * @param size
     *         The size of the noise.
     */
    private void checkNoiseSize(final int size) {
        if (size <= SystemConstants.ZERO) {
            throw new IllegalArgumentException("The size must be > 0!");
        }
        if ((size & size - 1) != SystemConstants.ZERO) {
            throw new IllegalArgumentException("The size must be a power of 2!");
        }
    }
}
