package ch.usi.inf.saiv.railwayempire.utilities;

import java.text.DecimalFormat;
import java.util.List;

import org.newdawn.slick.Color;
import org.newdawn.slick.Font;
import org.newdawn.slick.Graphics;

import ch.usi.inf.saiv.railwayempire.model.cells.CellTypes;
import ch.usi.inf.saiv.railwayempire.model.cells.ICell;
import ch.usi.inf.saiv.railwayempire.model.structures.StructureTypes;
import ch.usi.inf.saiv.railwayempire.utilities.FontUtils.Alignment;

/**
 * Utility Class for graphics related methods.
 * 
 */
public class GraphicsUtils {
    
    /**
     * Background color for boxes
     */
    private static final Color BOX_COLOR = new Color(0, 0, 0, .5f);
    
    /**
     * Padding for the boxes displaying costs.
     */
    private static final int COST_BOX_PADDING = 10;
    
    /**
     * Corner radius for the boxes displaying costs.
     */
    private static final int COST_BOX_CORNER_RADIUS = 5;
    
    /**
     * Currency format for costs and prices.
     */
    private static final DecimalFormat COST_FORMAT = new DecimalFormat("###,### $");
    
    /**
     * Utility class which holds static methods related to grpahics and rendering.
     * It cannot and should not be instanciated.
     */
    private GraphicsUtils() {
    }
    
    /**
     * Get the color of a single cell based on the content it holds as well as its neighbors.
     * 
     * If the cell has a structure whose color is defined then the color will be returned.
     * Otherwise the color of the terrain type will. This needs to be defined!
     * 
     * @param neighbors
     *            The neighbors of center cell.
     * @param center
     *            The cell to get the color from.
     * @return The color of the cell.
     */
    public static Color getCellColor(final List<ICell> neighbors, final ICell center) {
        final Color color;
        final ResourcesLoader resourcesLoader = ResourcesLoader.getInstance();
        if (GraphicsUtils.hasBuilding(neighbors)) {
            color = resourcesLoader.getColor("BUILDING");
        } else if (GraphicsUtils.hasTrack(neighbors)) {
            color = resourcesLoader.getColor("TRACK");
        } else if (GraphicsUtils.hasRiver(neighbors)) {
            color = resourcesLoader.getColor("RIVER");
        } else if (GraphicsUtils.hasMountain(neighbors)) {
            color = GraphicsUtils.getMountainColor(neighbors);
        } else {
            if (null == resourcesLoader.getColor(center.getTextureName())) {
                color = Color.pink;
            } else {
                color = resourcesLoader.getColor(center.getTextureName());
            }
        }
        return color;
    }
    
    /**
     * Check if there is a mountain in the given list of cells.
     * 
     * @param cells
     *            list of cells.
     * @return <code>true</code> if there is a mountain, <code>false</code> otherwise.
     */
    private static boolean hasMountain(final List<ICell> cells) {
        for (final ICell cell : cells) {
            if (cell.getCellType() == CellTypes.MOUNTAIN) {
                return true;
            }
        }
        return false;
    }
    
    private static Color getMountainColor(final List<ICell> cells) {
        for (final ICell cell : cells) {
            if (cell.getTextureName().startsWith(("NATURE_MOUNTAIN_2_"))) {
                return ResourcesLoader.getInstance().getColor("NATURE_MOUNTAIN_2_1");
            }
        }
        return ResourcesLoader.getInstance().getColor("NATURE_MOUNTAIN_1_1");
    }
    
    /**
     * Check if there is a river in the given list of cells.
     * 
     * @param cells
     *            list of cells.
     * @return <code>true</code> if there is a river, <code>false</code> otherwise.
     */
    private static boolean hasRiver(final List<ICell> cells) {
        for (final ICell cell : cells) {
            if (cell.getCellType() == CellTypes.RIVER) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * Check if there is a track in the given list of cells.
     * 
     * @param cells
     *            list of cells.
     * @return <code>true</code> if there is a track, <code>false</code> otherwise.
     */
    private static boolean hasTrack(final List<ICell> cells) {
        for (final ICell cell : cells) {
            if (cell.containsStructure() && cell.getStructure().getStructureType() == StructureTypes.TRACK) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * Check if there is a building in the given list of cells.
     * 
     * @param cells
     *            list of cells.
     * @return <code>true</code> if there is a building, <code>false</code> otherwise.
     */
    private static boolean hasBuilding(final List<ICell> cells) {
        for (final ICell cell : cells) {
            if (cell.containsStructure() && cell.getStructure().getStructureType() == StructureTypes.BUILDING) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * Get the color of a single cell based on it's content.
     * 
     * If the cell has a structure whose color is defined then the color will be returned.
     * Otherwise the color of the terrain type will. This needs to be defined!
     * 
     * @param cell
     *            The cell to get the color from.
     * @return The color of the cell.
     */
    public static Color getCellColor(final ICell cell) {
        Color color = null;
        final ResourcesLoader resourcesLoader = ResourcesLoader.getInstance();
        if (cell.containsStructure()) {
            color = resourcesLoader.getColor(cell.getStructure().getStructureType().toString());
        }
        if (null == color) {
            color = resourcesLoader.getColor(cell.getTextureName());
        }
        
        if (null == color) {
            throw new NullPointerException("The cell " + cell
                + "has a structure or a type whose color is not defined\n"
                + "Define a color for it in colors.xml\n"
                + "cell texture name was "
                + cell.getTextureName());
        }
        
        return color;
    }
    
    /**
     * Draw strings nicely formatted with shadow.
     * 
     * @param graphics
     *            graphics.
     * @param text
     *            string.
     * @param posX
     *            x coordinate.
     * @param posY
     *            y coordinate.
     */
    public static void drawShadowText(final Graphics graphics, final String text, final float posX, final float posY) {
        graphics.setColor(Color.black);
        graphics.drawString(text, posX + 2, posY + 2);
        graphics.setColor(Color.white);
        graphics.drawString(text, posX, posY);
    }
    
    /**
     * Draw strings nicely formatted with shadow and the given allignment.
     * 
     * @param graphics
     *            The graphics context.
     * @param text
     *            The text to draw
     * @param posX
     *            The x position of the string.
     * @param posY
     *            The y position of the string.
     * @param width
     *            The width to fill with the string.
     * @param alignment
     *            the alignment of the text.
     */
    public static void drawShadowText(final Graphics graphics,
        final String text,
        final int posX,
        final int posY,
        final int width,
        final Alignment alignment) {
        GraphicsUtils.drawShadowText(graphics, text, posX, posY, width, alignment, graphics.getFont());
    }
    
    /**
     * Draw strings nicely formatted with shadow and the given allignment.
     * 
     * @param graphics
     *            The graphics context.
     * @param text
     *            The text to draw
     * @param posX
     *            The x position of the string.
     * @param posY
     *            The y position of the string.
     * @param width
     *            The width to fill with the string.
     * @param alignment
     *            the alignment of the text.
     * 
     * @param font
     *            The font used to draw the text.
     */
    public static void drawShadowText(final Graphics graphics,
        final String text,
        final int posX,
        final int posY,
        final int width,
        final Alignment alignment, final Font font) {
        
        FontUtils.drawString(font, text, alignment, posX + 2, posY + 2, width, Color.black);
        FontUtils.drawString(font, text, alignment, posX, posY, width, Color.white);
    }
    
    /**
     * Draw a nice transparent box with rounded borders.
     * 
     * @param graphics
     *            graphics object.
     * @param text
     *            text to draw.
     * @param posX
     *            x coordinate of topleft corner.
     * @param posY
     *            y coordinate of topleft corner.
     * @param width
     *            width of bounding rectangle.
     * @param height
     *            height of bounding rectangle.
     * @param cornerRadius
     *            radius of rounded corners.
     * @param margin
     *            margin for the string.
     */
    public static void drawBox(final Graphics graphics,
        final String text,
        final int posX,
        final int posY,
        final int width,
        final int height,
        final int cornerRadius,
        final int margin) {
        
        graphics.setColor(GraphicsUtils.BOX_COLOR);
        graphics.fillRoundRect(posX, posY, width, height, cornerRadius);
        graphics.drawRoundRect(posX, posY, width - 1, height - 1, cornerRadius);
        graphics.setColor(Color.white);
        graphics.drawString(text, posX + margin, posY + margin);
    }
    
    /**
     * Draw a nice box for the cost of something.
     * 
     * @param graphics
     *            graphics object.
     * @param cost
     *            cost of that thing.
     * @param posX
     *            x coordinate.
     * @param posY
     *            y coordinate.
     */
    public static void drawCostBox(final Graphics graphics, final long cost, final int posX, final int posY) {
        final String costString = "-" + GraphicsUtils.formatCost(cost);
        final int stringWidth = graphics.getFont().getWidth(costString);
        final int width = stringWidth + 2 * GraphicsUtils.COST_BOX_PADDING;
        final int height = graphics.getFont().getLineHeight() + 2 * GraphicsUtils.COST_BOX_PADDING;
        
        graphics.setColor(GraphicsUtils.BOX_COLOR);
        graphics.fillRoundRect(posX, posY, width, height, GraphicsUtils.COST_BOX_CORNER_RADIUS);
        graphics.drawRoundRect(posX, posY, width - 1, height - 1, GraphicsUtils.COST_BOX_CORNER_RADIUS);
        
        FontUtils.drawCenter(graphics.getFont(),
            costString,
            posX + GraphicsUtils.COST_BOX_PADDING,
            posY + GraphicsUtils.COST_BOX_PADDING,
            stringWidth,
            Color.white);
    }
    
    /**
     * Format the cost into a pretty string.
     * @param cost The cost to format.
     * @return The string of the formated cost.
     */
    public static String formatCost(final long cost) {
        return GraphicsUtils.COST_FORMAT.format(cost);
    }
}
