package ch.usi.inf.saiv.railwayempire.utilities;

/**
 * TimeObserver. Register this to Time to receive updates.
 */
public interface TimeObserver {
    /**
     * Update method.
     * 
     * @param delta
     *        in game milliseconds elapsed since last update.
     */
    void update(final long delta);
}
