package ch.usi.inf.saiv.railwayempire.gui.elements.center.route;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Polygon;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.gui.GUIContext;

import ch.usi.inf.saiv.railwayempire.gui.elements.AbstractGuiComponent;
import ch.usi.inf.saiv.railwayempire.model.structures.Station;
import ch.usi.inf.saiv.railwayempire.model.structures.StationSizes;
import ch.usi.inf.saiv.railwayempire.utilities.GraphicsUtils;
import ch.usi.inf.saiv.railwayempire.utilities.ResourcesLoader;
import ch.usi.inf.saiv.railwayempire.utilities.SystemConstants;

/**
 * Interactive Icon representing a train station
 * 
 */
public class TrainStationIcon extends AbstractGuiComponent {
    
    /**
     * Reference to the resource loader.
     */
    private static final ResourcesLoader loader = ResourcesLoader.getInstance();
    
    /**
     * The panel holding the icon.
     */
    private final TrainRoutePanel panel;
    
    /**
     * the station represented by the icon.
     */
    private final Station station;
    
    /**
     * All the states of the icon, based on the station and it's presence (or not) in the current train's route.
     * 
     */
    public enum State {
        /**
         * A station not linked to the railway network.
         */
        UNREACHABLE,
            
            /**
             * A station linked to the railway network but not reachable at this time.
             */
            LINKED,
            
            /**
             * A station which can be reached by the train.
             */
            REACHABLE,
            
            /**
             * The last station on the train's route.
             */
            LATEST,
            
            /**
             * A station which is already part of the train route.
             */
            SELECTED
    }
    
    /**
     * The current state of the icon.
     */
    private State state;
    
    /**
     * The default shape of the icon.
     */
    private final Shape defaultShape;
    
    /**
     * The highlight shape of the icon.
     */
    private final Shape highlightShape;
    
    /**
     * Indicates if the name needs to be drawn or not.
     */
    private boolean drawName;
    
    /**
     * Instantiate a new icon for the given station.
     * 
     * @param newStation
     *            The station for which to generate the icon.
     * 
     * @param trainRoutePanel
     *            The panel which contains the icon.
     */
    public TrainStationIcon(final Station newStation, final TrainRoutePanel trainRoutePanel) {
        super(TrainStationIcon.generateShape(newStation.getSize(), newStation.getRadius(), false));
        this.station = newStation;
        this.state = this.station.isLinked() ? State.LINKED : State.UNREACHABLE;
        this.panel = trainRoutePanel;
        this.defaultShape = this.getShape();
        this.highlightShape = TrainStationIcon.generateShape(newStation.getSize(), newStation.getRadius(), true);
        this.defaultShape.setCenterX(this.station.getX());
        this.defaultShape.setCenterY(this.station.getY());
        this.highlightShape.setCenterX(this.station.getX());
        this.highlightShape.setCenterY(this.station.getY());
        this.drawName = false;
        
    }
    
    /**
     * Generate the shape of the icon based on the station.
     * 
     * @param stationSize
     *            The size of the station which will determine the shape.
     * 
     * @param iconWidth
     *            The width of the icon.
     * @param big
     *            If <code>true</code>, creates a s shape of bigger size (used for highlight);
     * 
     * @return The shape generated based on the given station.
     */
    protected static Shape generateShape(final StationSizes stationSize, final float iconWidth, final boolean big) {
        Shape shape = null;
        final float width = iconWidth * (big ? 4 : 2) + 1;
        switch (stationSize) {
            case LARGE:
                final Float sqrt3 = (float) Math.sqrt(3);
                shape = new Polygon(new float[] {
                    0, width / sqrt3,
                    -width / 6, width / (2 * sqrt3),
                    -width / 2, width / (2 * sqrt3),
                    -width / 3, 0,
                    -width / 2, -width / (2 * sqrt3),
                    -width / 6, -width / (2 * sqrt3),
                    0, -width / sqrt3,
                    width / 6, -width / (2 * sqrt3),
                    width / 2, -width / (2 * sqrt3),
                    width / 3, 0,
                    width / 2, width / (2 * sqrt3),
                    width / 6, width / (2 * sqrt3) });
                break;
            case MEDIUM:
                shape = new Polygon(new float[] {
                    iconWidth * (big ? 2 : 1), 0,
                    width - 1, iconWidth * (big ? 2 : 1),
                    iconWidth * (big ? 2 : 1), width - 1,
                    0, iconWidth * (big ? 2 : 1) });
                break;
            case SMALL:
                shape = new Rectangle(0, 0, width + 1, width + 1);
                break;
            default:
                break;
        }
        return shape;
    }
    
    /**
     * 
     * {@inheritDoc}
     */
    @Override
    public void render(final GUIContext container, final Graphics graphics) {
        graphics.setColor(TrainStationIcon.loader.getColor("STATION_" + this.state));
        graphics.fill(this.getShape());
        if (this.drawName) {
            final float nameWidth = graphics.getFont().getWidth(this.station.getName());
            GraphicsUtils.drawShadowText(graphics,
                this.station.getName(),
                this.getX() - 0.5f * nameWidth,
                this.getY() - 20);
        }
        
    }
    
    /**
     * 
     * {@inheritDoc}
     */
    @Override
    public void mouseClicked(final int button, final int coordX, final int coordY, final int clickCount) {
        // Implement if needed.
        
    }
    
    /**
     * 
     * {@inheritDoc}
     */
    @Override
    public void mousePressed(final int button, final int coordX, final int coordY) {
        // Implement if needed.
        
    }
    
    /**
     * 
     * {@inheritDoc}
     */
    @Override
    public void mouseReleased(final int button, final int coordX, final int coordY) {
        switch (button) {
            case SystemConstants.SELECT_BUTTON:
                if (this.state == State.REACHABLE || this.state == State.SELECTED) {
                    this.panel.addStation(this.station);
                }
                break;
            case SystemConstants.UNSELECT_BUTTON:
                throw new UnsupportedOperationException("Removal of stations is not yet supported");
            default:
                break;
        }
    }
    
    /**
     * Set the sate of the icon
     * 
     * @param newState
     *            The new state of the icon.
     */
    public void setState(final State newState) {
        if (this.state == State.SELECTED && newState == State.REACHABLE) {
            return;
        }
        this.state = newState;
        
    }
    
    /**
     * Reset the state of the icon.
     */
    public void resetState() {
        this.state = this.station.isLinked() ? State.LINKED : State.UNREACHABLE;
        
    }
    
    /**
     * Set the icon representation as the latest on a train route
     */
    public void latest() {
        this.setState(State.LATEST);
        
    }
    
    /**
     * Get the state of the icon.
     * 
     * @return The state of the icon.
     */
    public State getState() {
        return this.state;
    }
    
    /**
     * Get the station represented by the icon.
     * 
     * @return The station represented by the icon.
     */
    public Station getStation() {
        return this.station;
    }
    
    /**
     * Highlight this icon.
     */
    public void highlight() {
        this.setShape(this.highlightShape);
        this.drawName = true;
    }
    
    /**
     * Highlight this icon.
     */
    public void unhighlight() {
        this.setShape(this.defaultShape);
        this.drawName = false;
    }
    
    /**
     * 
     * {@inheritDoc}
     */
    @Override
    public void mouseDragged(final int oldX, final int oldY, final int newX, final int newY) {
        // Implement if needed.
        
    }
    
    /**
     * 
     * {@inheritDoc}
     */
    @Override
    public void mouseMoved(final int oldX, final int oldY, final int newX, final int newY) {
        if (this.getShape().contains(newX, newY)) {
            this.highlight();
        } else if (this.getShape().contains(oldX, oldY)) {
            this.unhighlight();
        }
        
    }
    
    /**
     * 
     * {@inheritDoc}
     */
    @Override
    public void mouseWheelMoved(final int charge, final int coordX, final int coordY) {
        // Implement if needed.
        
    }
    
    /**
     * 
     * {@inheritDoc}
     */
    @Override
    public void keyPressed(final int key, final char character) {
        // Implement if needed.
        
    }
    
    /**
     * 
     * {@inheritDoc}
     */
    @Override
    public void setX(final int newX) {
        this.defaultShape.setX(newX);
        this.highlightShape.setX(newX);
        super.setX(newX);
    }
    
    /**
     * 
     * {@inheritDoc}
     */
    @Override
    public void setY(final int newY) {
        this.defaultShape.setY(newY);
        this.highlightShape.setY(newY);
        super.setY(newY);
    }
    
    /**
     * 
     * {@inheritDoc}
     */
    @Override
    public void setCenterX(final float newX) {
        this.defaultShape.setCenterX(newX);
        this.highlightShape.setCenterX(newX);
        super.setCenterX(newX);
    }
    
    /**
     * 
     * {@inheritDoc}
     */
    @Override
    public void setCenterY(final float newY) {
        this.defaultShape.setCenterY(newY);
        this.highlightShape.setCenterY(newY);
        super.setCenterY(newY);
    }
    
}
