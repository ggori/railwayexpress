package ch.usi.inf.saiv.railwayempire.utilities.serialization;


import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.List;

import ch.usi.inf.saiv.railwayempire.model.Game;
import ch.usi.inf.saiv.railwayempire.utilities.Log;
import ch.usi.inf.saiv.railwayempire.utilities.SystemConstants;


/**
 * A representation fo the object responsible for the loading of the saved games.
 */
public final class Load {

    /**
     * The default folder on which to save the games.
     */
    private static final String SAVES_FOLDER = "saves/";
    /**
     * The unique instance of the save.
     */
    private static final Load UNIQUE_INSTANCE = new Load();

    /**
     * Creates a new load object responsible for serialization.
     */
    private Load() {
    }

    /**
     * Returns the unique instance of the save.
     *
     * @return The instance of the save.
     */
    public static Load getInstance() {
        return UNIQUE_INSTANCE;
    }

    /**
     * Loads a saved game.
     *
     * @param timeStamp
     *         The timestamp of the game to load.
     * @return The loaded instance of the game.
     */
    public Game loadGame(final String timeStamp) {
        Game game = null;
        try {
            final String toLoad = SAVES_FOLDER + timeStamp;
            final FileInputStream fileIn = new FileInputStream(toLoad);
            final ObjectInputStream in = new ObjectInputStream(fileIn);
            game = (Game) in.readObject();
            in.close();
            fileIn.close();
        } catch (IOException exception) {
            Log.exception(exception);
        } catch (ClassNotFoundException exception) {
            Log.exception(exception);
        }
        return game;
    }

    /**
     * Returns the list of saved games.
     *
     * @return The list of saved games.
     */
    public List<String> getSavedGamesList() {
        final File folder = new File(SystemConstants.SAVE_PATH);
        final File[] listOfFiles = folder.listFiles();
        final List<String> filenameList = new ArrayList<String>();
        if (listOfFiles != null) {
            for (final File file : listOfFiles) {
                if (file.isFile() && file.getName().endsWith(".ser")) {
                    filenameList.add(file.getName().substring(0, file.getName().lastIndexOf('.')));
                }
            }
        }
        return filenameList;
    }
}
