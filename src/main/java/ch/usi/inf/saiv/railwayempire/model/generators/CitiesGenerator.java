package ch.usi.inf.saiv.railwayempire.model.generators;


import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import ch.usi.inf.saiv.railwayempire.model.datastructures.Graph;
import ch.usi.inf.saiv.railwayempire.model.City;
import ch.usi.inf.saiv.railwayempire.model.Terrain;
import ch.usi.inf.saiv.railwayempire.model.cells.ICell;
import ch.usi.inf.saiv.railwayempire.model.production_sites.Building;
import ch.usi.inf.saiv.railwayempire.model.structures.StructureTypes;
import ch.usi.inf.saiv.railwayempire.model.structures.Track;
import ch.usi.inf.saiv.railwayempire.model.structures.TrackTypes;
import ch.usi.inf.saiv.railwayempire.utilities.GameConstants;
import ch.usi.inf.saiv.railwayempire.utilities.SystemConstants;


/**
 * Cities generator.
 */
public final class CitiesGenerator {

    /**
     * The radius in which to spawn buildings.
     */
    private static final int BUILDINGS_SPAWN_RADIUS = 2;
    /**
     * The radius in which to cut forest around city.
     */
    private static final int CUT_FOREST_RADIUS = 8;

    /**
     * The empty constructor for the generator. Never to be called.
     */
    private CitiesGenerator() {
    }

    /**
     * Generates a list of cities, randomly placed on the terrain.
     *
     * @param terrain
     *         The terrain onA which to place the cities.
     * @return A list of the cities.
     */
    public static List<City> generateCities(final Terrain terrain) {
        final Random random = new Random();

        if (terrain == SystemConstants.NULL) {
            throw new IllegalArgumentException("The terrain was null!");
        }
        final List<City> cities = new ArrayList<City>();

        for (int i = 0; i < GameConstants.CITIES_ITERATION_LIMIT  && cities.size() < GameConstants.MAX_CITIES; ++i) {
            final int posX = random.nextInt(terrain.getSize());
            final int posY = random.nextInt(terrain.getSize());

            final ICell cell = terrain.getCell(posX, posY);
            if (cell.canBuildBuilding() && !cell.containsStructure() &&
                    !CitiesGenerator.isCityNearby(posX, posY, cities)) {

                final City newCity = new City(posX, posY);
                cities.add(newCity);

                final Building building = new Building(newCity.getX(), newCity.getY(),
                        GameConstants.BUILDING_PRODUCTION_RATE, GameConstants.BUILDING_MAX_UNIT, newCity);

                terrain.getCell(newCity.getX(), newCity.getY()).setStructure(building);
            }
        }
        return cities;
    }

    /**
     * Checks it a city is close to the received point on the map. The minimum
     * distance between cities is used to do the comparison. This can be found
     * in the constants class.
     *
     * @param posX
     *         The x coordinate of the point to check.
     * @param posY
     *         The y coordinate of the point to check.
     * @param cities
     *         The list of the currently existing cities.
     * @return True if there is a city near the point, false otherwise.
     */
    private static boolean isCityNearby(final int posX, final int posY, final List<City> cities) {
        for (final City city : cities) {
            if (city.distanceFrom(posX, posY) < GameConstants.MIN_CITIES_DISTANCE) {
                return true;
            }
        }
        return false;
    }

    /**
     * Places the three track pieces from which the player can start the game.
     * The three tracks are placed near a random city.
     *
     * @param terrain
     *         The current terrain.
     * @param cities
     *         The current list of cities.
     * @return True of it was possible to place the tracks, false otherwise.
     */
    public static boolean placeStartingTracks(final Terrain terrain, final List<City> cities) {
        if (terrain == SystemConstants.NULL) {
            throw new IllegalArgumentException("The terrain on which to place cities cannot be null!");
        }
        if (cities == SystemConstants.NULL) {
            throw new IllegalArgumentException("The list of cities cannot be null!");
        }
        if (cities.isEmpty()) {
            throw new IllegalArgumentException("The list of cities must contain at least one city!");
        }

        for (final City city : cities) {
            final int posX = city.getX();
            final int posY = city.getY();

            final int[] placements = {-1, 0, 1};
            final int[] minus1 = {-1};
            final int[] plus1 = {1};

            if (CitiesGenerator.checkTriple(terrain, posX, posY, placements, minus1)
                    || CitiesGenerator.checkTriple(terrain, posX, posY, placements, plus1)
                    || CitiesGenerator.checkTriple(terrain, posX, posY, minus1, placements)
                    || CitiesGenerator.checkTriple(terrain, posX, posY, plus1, placements)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Checks if the three points are all available for the placement of
     * tracks.
     * If they are it places the three starting tracks.
     *
     * @param terrain
     *         The current terrain.
     * @param posX
     *         The x coordinate of the city around which we want to to
     *         place the tracks.
     * @param posY
     *         The x coordinate of the city around which we want to to
     *         place the tracks.
     * @param range1
     *         The range of x coordinates to check.
     * @param range2
     *         The range of y coordinates to check.
     * @return True if the tracks have been placed, false otherwise.
     */
    private static boolean checkTriple(final Terrain terrain, final int posX, final int posY,
                                       final int[] range1, final int[] range2) {

        for (final int relX : range1) {
            final int testX = posX + relX;
            for (final int relY : range2) {
                final int testY = posY + relY;
                if (!CitiesGenerator.isValidTrackPosition(terrain, testX, testY)) {
                    return false;
                }
            }
        }

        final Graph<Track> tracksGraph = terrain.getTrackGraph();

        Track midTrack = null;
        Track oldTrack = null;
        for (final int relX : range1) {
            final int testX = posX + relX;
            for (final int relY : range2) {
                final int testY = posY + relY;
                final Track newTrack = new Track(testX, testY, tracksGraph, TrackTypes.NORMAL);
                if (oldTrack != SystemConstants.NULL) {
                    if (midTrack == SystemConstants.NULL) {
                        midTrack = newTrack;
                    }
                    oldTrack.addNeighbor(newTrack);
                    newTrack.addNeighbor(oldTrack);
                }
                final ICell cell = terrain.getCell(testX, testY);
                if (cell.containsStructure() && cell.getStructure().getStructureType() == StructureTypes.FOREST) {
                    cell.removeStructure();
                }
                cell.setStructure(newTrack);
                oldTrack = newTrack;
            }
        }
        return true;
    }

    /**
     * Checks if the received point is a valid position for a track.
     *
     * @param terrain
     *         The current terrain.
     * @param posX
     *         The x coordinate of the point.
     * @param posY
     *         The y coordinate of the point.
     * @return True if the point is a valid position, false otherwise.
     */
    private static boolean isValidTrackPosition(final Terrain terrain, final int posX, final int posY) {
        return terrain.areCoordinatesInRange(posX, posY) && terrain.getCell(posX, posY).canBuildTrack();
    }

    /**
     * Method that takes a city and places some buildings in it.
     *
     * @param newCity
     *         city.
     * @param terrain
     *         terrain.
     */
    public static void spawnBuildings(final City newCity, final Terrain terrain) {
        for (int x = newCity.getX() - CitiesGenerator.BUILDINGS_SPAWN_RADIUS;
             x < newCity.getX() + CitiesGenerator.BUILDINGS_SPAWN_RADIUS; ++x) {

            for (int y = newCity.getY() - CitiesGenerator.BUILDINGS_SPAWN_RADIUS;
                 y < newCity.getY() + CitiesGenerator.BUILDINGS_SPAWN_RADIUS; ++y) {

                if (terrain.areCoordinatesInRange(x, y)) {
                    final ICell cell = terrain.getCell(x, y);
                    if (cell.canBuildBuilding() && !cell.containsStructure() && Math.random() < 0.3) {
                        final Building building = new Building(x, y, GameConstants.BUILDING_PRODUCTION_RATE,
                                GameConstants.BUILDING_MAX_UNIT, newCity);
                        terrain.getCell(x, y).setStructure(building);
                    }
                }
            }
        }
        CitiesGenerator.cutForestAroundCity(terrain, newCity.getX(), newCity.getY());
    }

    /**
     * Method that cuts forest around the city.
     *
     * @param terrain
     *         terrain.
     * @param posX
     *         city coordinate x.
     * @param posY
     *         city coordinate y.
     */
    public static void cutForestAroundCity(final Terrain terrain, final int posX, final int posY) {
        final int startX = posX - CitiesGenerator.CUT_FOREST_RADIUS / 2;
        final int startY = posY - CitiesGenerator.CUT_FOREST_RADIUS / 2;
        final int endX = posX + CitiesGenerator.CUT_FOREST_RADIUS / 2;
        final int endY = posY + CitiesGenerator.CUT_FOREST_RADIUS / 2;
        for (int currentX = startX; currentX <= endX; currentX++) {
            for (int currentY = startY; currentY <= endY; currentY++) {
                final ICell nextCell = terrain.getCell(currentX, currentY);
                if (nextCell.containsStructure() && nextCell.getStructure().isRemovable()) {
                    nextCell.removeStructure();
                }
            }
        }
    }
}
