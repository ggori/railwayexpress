package ch.usi.inf.saiv.railwayempire.gui.elements.bottom;

import org.newdawn.slick.geom.Rectangle;

import ch.usi.inf.saiv.railwayempire.gui.elements.AbstractPanel;


/**
 * World info utility panel.
 */
public abstract class AbstractWorldInfoUtilityPanel extends AbstractPanel {
    
    /**
     * Constructor.
     * 
     * @param rectangle
     *            shape.
     */
    public AbstractWorldInfoUtilityPanel(final Rectangle rectangle) {
        super(rectangle);
    }
}
