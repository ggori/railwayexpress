package ch.usi.inf.saiv.railwayempire.gui.elements;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.UnicodeFont;
import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.gui.GUIContext;

import ch.usi.inf.saiv.railwayempire.model.Game;
import ch.usi.inf.saiv.railwayempire.model.Player;
import ch.usi.inf.saiv.railwayempire.utilities.FontUtils;
import ch.usi.inf.saiv.railwayempire.utilities.GameConstants;
import ch.usi.inf.saiv.railwayempire.utilities.ResourcesLoader;
import ch.usi.inf.saiv.railwayempire.utilities.StatisticsManager;

/**
 * Panel that displays informations at the end of the game.
 */
public class EndGamePanel extends SimplePanel {
    /*
     * The rank names.
     */
    private static final String[] RANKS = {"AZ",
        "President",
        "Dean",
        "Full Prof",
        "Associate Prof",
        "Senior Assistant Prof",
        "Junior Assistant Prof",
        "Postdoc",
        "PhD Student",
        "MSc Student",
        "BSc Student",
        "Helpdesker", };
    
    /**
     * The rank thresholds.
     */
    private static final int[] RANKPOINTS = {150000,
        100000,
        75000,
        45000,
        30000,
        20000,
        15000,
        10000,
        5000,
        3000,
        1000,
        0, };
    
    /**
     * The width of the description string for each statistic.
     */
    private static final int DESCRIPTION_WIDTH = 750;
    /**
     * The width of the score part.
     */
    private static final int SCORE_WIDTH = 250;
    /**
     * The margin from top of the screen.
     */
    private static final int TOP_MARGIN = 200;
    /**
     * The margin of the container for the text.
     */
    private static final int CONTAINER_MARGIN = 100;
    /**
     * The font used to draw.
     */
    private static final UnicodeFont FONT = FontUtils.resizeFont(FontUtils.FONT, 23f);
    /**
     * The font for the title.
     */
    private static final UnicodeFont TITLE_FONT = FontUtils.resizeFont(FontUtils.FONT, 30f);
    /**
     * The height of a line of text.
     */
    private static final int LINE_HEIGHT = EndGamePanel.FONT.getLineHeight();
    /**
     * The horizontal offset in order to center the scores.
     */
    private final int xOffset;
    /**
     * The number of written lines. (reset at each render).
     */
    private int writtenLines;
    
    /**
     * Constructor. Takes its shape and the slick2d game container.
     * 
     * @param newShape
     *            shape of the panel.
     * @param container
     *            slick2d game container.
     */
    public EndGamePanel(final Shape newShape, final GameContainer container) {
        super(newShape);
        this.xOffset = (container.getWidth() - EndGamePanel.DESCRIPTION_WIDTH - EndGamePanel.SCORE_WIDTH) / 2;
        
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void render(final GUIContext container, final Graphics graphics) {
        super.render(container, graphics);
        
        final Game game = Game.getInstance();
        final Player player = game.getPlayer();
        final StatisticsManager statisticsManager = StatisticsManager.getInstance();
        
        final ResourcesLoader loader = ResourcesLoader.getInstance();
        loader.getImage("SCOREBOARD_BACKGROUND").draw(0, 0, container.getWidth(), container.getHeight());
        
        graphics.setColor(new Color(0f, 0f, 0f, 0.9f));
        graphics.fillRoundRect(this.xOffset - EndGamePanel.CONTAINER_MARGIN, EndGamePanel.TOP_MARGIN - EndGamePanel.CONTAINER_MARGIN, container.getWidth() - 2
            * (this.xOffset - EndGamePanel.CONTAINER_MARGIN), container.getHeight() - 1.5f * EndGamePanel.TOP_MARGIN, 20);
        
        FontUtils.drawCenter(EndGamePanel.TITLE_FONT, "Empire Points Overview", 0, EndGamePanel.TOP_MARGIN - EndGamePanel.CONTAINER_MARGIN / 2,
            container.getWidth());
        this.write("Total money possessed", player.getAvailableMoney(), graphics);
        this.minus("Initial money", GameConstants.INITIAL_MONEY, graphics);
        this.writtenLines++;
        
        this.write("Number of owned trains", game.getWorld().getTrainManager().getTrains().size(), graphics);
        this.writtenLines++;
        
        this.write("Number of owned stations", game.getWorld().getStationManager().getStations().size(),
            graphics);
        this.times("", GameConstants.POINTS_FOR_LINKED_STATION, graphics);
        this.writtenLines++;
        
        this.plus("Points from traded wares", statisticsManager.pointsFromTradedWares(), graphics);
        this.plus("Points for cities reached", statisticsManager.pointsForCitiesReached(), graphics);
        this.writtenLines++;
        
        this.write("Total Empire Points", statisticsManager.computeScore(), graphics);
        
        this.writtenLines++;
        
        int rankIndex = 0;
        while (statisticsManager.computeScore() < EndGamePanel.RANKPOINTS[rankIndex]) {
            rankIndex++;
        }
        this.write("Rank", EndGamePanel.RANKS[rankIndex], graphics);
        
        this.writtenLines = 0;
        
        FontUtils.drawCenter(EndGamePanel.TITLE_FONT, "Press any key to continue", 0,
            (int) (container.getHeight() - 1.5f * EndGamePanel.TOP_MARGIN),
            container.getWidth());
    }
    
    /**
     * Draws the description and relative score in green, adding a "plus" sign
     * before the score.
     * 
     * @param string
     *            The description of the score.
     * @param value
     *            The score.
     * @param graphics
     *            The graphics to draw on.
     */
    private void plus(final String string, final long value, final Graphics graphics) {
        this.write(string, "+ " + value, graphics, new Color(20, 210, 0));
    }
    
    /**
     * Draws the description and relative score in green, adding a "x"
     * (multiplication) sign before the score.
     * 
     * @param string
     *            The description of the score.
     * @param value
     *            The score.
     * @param graphics
     *            The graphics to draw on.
     */
    private void times(final String string, final long value, final Graphics graphics) {
        this.write(string, "x " + value, graphics, new Color(20, 210, 0));
    }
    
    /**
     * Draws the description and relative score in red, adding a "minus" sign
     * before the score.
     * 
     * @param string
     *            The description of the score.
     * @param value
     *            The score.
     * @param graphics
     *            The graphics to draw on.
     */
    private void minus(final String string, final long value, final Graphics graphics) {
        this.write(string, "- " + value, graphics, new Color(224, 0, 0));
    }
    
    /**
     * Draws the description and relative score in white.
     * 
     * @param string
     *            The description of the score.
     * @param value
     *            The score.
     * @param graphics
     *            The graphics to draw on.
     */
    private void write(final String string, final long value, final Graphics graphics) {
        this.write(string, "" + value, graphics, Color.white);
    }
    
    /**
     * Draws the description and relative score in white.
     * 
     * @param string
     *            The description of the score.
     * @param string
     *            The score.
     * @param graphics
     *            The graphics to draw on.
     */
    private void write(final String string, final String value, final Graphics graphics) {
        this.write(string, "" + value, graphics, Color.white);
    }
    
    /**
     * Draws the description and relative score in the given color.
     * 
     * @param string
     *            The description of the score.
     * @param value
     *            The score.
     * @param graphics
     *            The graphics to draw on.
     */
    private void write(final String string, final String value, final Graphics graphics, final Color color) {
        final int vOffset = EndGamePanel.TOP_MARGIN + this.writtenLines++ * EndGamePanel.LINE_HEIGHT;
        FontUtils.drawLeft(EndGamePanel.FONT, string, this.xOffset, vOffset, color);
        FontUtils.drawRight(EndGamePanel.FONT, value, this.xOffset + EndGamePanel.DESCRIPTION_WIDTH, vOffset, EndGamePanel.SCORE_WIDTH, color);
    }
    
}
