package ch.usi.inf.saiv.railwayempire.model.structures;

import java.util.ArrayList;
import java.util.List;

import ch.usi.inf.saiv.railwayempire.model.Game;
import ch.usi.inf.saiv.railwayempire.model.generators.NamesGenerator;
import ch.usi.inf.saiv.railwayempire.model.City;
import ch.usi.inf.saiv.railwayempire.model.cells.CellTypes;
import ch.usi.inf.saiv.railwayempire.model.cells.ICell;
import ch.usi.inf.saiv.railwayempire.model.production_sites.IProductionSite;
import ch.usi.inf.saiv.railwayempire.model.wagons.IWagon;
import ch.usi.inf.saiv.railwayempire.model.stores.IBuyable;
import ch.usi.inf.saiv.railwayempire.utilities.GameConstants;
import ch.usi.inf.saiv.railwayempire.utilities.Log;
import ch.usi.inf.saiv.railwayempire.utilities.SystemConstants;

/**
 * Station Class.
 */
public final class Station extends AbstractStructure implements IBuyable {
    
    /**
     * serialVersionUID generated by eclipse.
     */
    private static final long serialVersionUID = 2722223580866960130L;
    /**
     * The name of the station.
     */
    private final String name;
    /**
     * The city to which this station belongs.
     */
    private final City city;
    /**
     * Collection of the extraction sites inside the stations effect area.
     */
    private final List<IProductionSite> productionSites;
    /**
     * Track linked to the station.
     */
    private Track track;
    /**
     * Radius of the station's area of interest.
     */
    private int radius;
    /**
     * The size of the station: SMALL, MEDIUM, LARGE.
     */
    private final StationSizes size;

    /**
     * Indicates if a station is linked to the network or not.
     */
    private boolean linked;
    
    /**
     * Station constructor.
     *
     * @param newX
     *            The x coordinate of the station.
     * @param newY
     *            The y coordinate of the station.
     * @param stationDimension
     *            Size of the station.
     */
    public Station(final int newX, final int newY, final StationSizes stationDimension) {
        super(newX, newY, "STATION_"+stationDimension.name()+"_0");
        this.linked = false;
        this.size = stationDimension;
        this.city = this.findClosestCity();
        if (newX != 0 && newY != 0) {
            this.name = NamesGenerator.getInstance().generateStationName(this.city.getName());
        } else {
            this.name = "some";
        }
        switch (this.size) {
            case SMALL:
                this.radius = GameConstants.SMALL_STATION_RADIUS;
                break;
            case MEDIUM:
                this.radius = GameConstants.MEDIUM_STATION_RADIUS;
                break;
            case LARGE:
                this.radius = GameConstants.LARGE_STATION_RADIUS;
                break;
            default:
                throw new IllegalArgumentException("Station size not known!");
        }
        
        this.productionSites = new ArrayList<IProductionSite>();
        this.productionSites.addAll(this.retrieveNearbyExtractionSites(Game.getInstance().getWorld()
            .getProductionSites()));
    }

    /**
     * Returns the city to which the station belongs.
     * @return The city.
     */
    public City getCity() {
        return this.city;
    }

    /**
     * Add a new production site to the list of them.
     *
     * @param productionSite
     *            the production site to be add.
     */
    public void addProductionSites(final IProductionSite productionSite) {
        this.productionSites.add(productionSite);
    }
    
    /**
     * Override method to return this.
     *
     * @return this station.
     */
    @Override
    public Station asStation() {
        return this;
    }
    
    /**
     * If possible, find a neighboring track and attach it to the station.
     *
     * @return true if successful, false if it didn't attach anything.
     */
    public boolean attachTracks() {
        if (this.track == SystemConstants.NULL) {
            for (final ICell cell : Game.getInstance().getWorld().getTerrain()
                .getCloseNeighborCells(this.getX(), this.getY())) {
                if (cell.getCellType() == CellTypes.GROUND && cell.containsStructure()) {
                    final Track newTrack = cell.getStructure().asTrack();
                    if (newTrack != SystemConstants.NULL) {
                        this.track = newTrack;
                        this.track.setStation(this);
                        if (this.getY() == this.track.getY()) {
                            this.setDirection(1);
                        } else {
                            this.setDirection(0);
                        }
                        Game.getInstance().getWorld().getStationManager().notifyLinked(this);
                        Log.screen("Linked Track to " + this.getName());
                        return true;
                    }
                }
            }
        }
        return false;
    }
    
    /**
     * Computes the distance from this station to the given city.
     *
     * @param city
     *            The city to which to calculate the distance.
     * @return The distance.
     */
    private double distanceWithCity(final City city) {
        final double deltaX = Math.pow(city.getX() - this.getX(), 2);
        final double deltaY = Math.pow(city.getY() - this.getY(), 2);
        return Math.sqrt(deltaX + deltaY);
    }
    
    /**
     * Finds the closest city to the station.
     *
     * @return The closest city.
     */
    private City findClosestCity() {
        City parent = Game.getInstance().getWorld().getCities().get(0);
        
        for (final City city : Game.getInstance().getWorld().getCities()) {
            if (this.distanceWithCity(city) < this.distanceWithCity(parent)) {
                parent = city;
            }
        }
        return parent;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public int getCost() {
        final int retCost;
        switch (this.size) {
            case SMALL:
                retCost = GameConstants.SMALL_STATION_COST;
                break;
            case MEDIUM:
                retCost = GameConstants.MEDIUM_STATION_COST;
                break;
            case LARGE:
                retCost = GameConstants.LARGE_STATION_COST;
                break;
            default:
                retCost = -1;
                break;
        }
        return retCost;
    }
    
    /**
     * Returns the name of the station.
     *
     * @return The name of the station.
     */
    public String getName() {
        return this.name;
    }
    
    /**
     * Returns the production sites that the station can reach.
     *
     * @return The production sites.
     */
    public final List<IProductionSite> getProductionSites() {
        return this.productionSites;
    }
    
    /**
     * Getter for the station's radius.
     *
     * @return Integer value of the radius.
     */
    public final int getRadius() {
        return this.radius;
    }
    
    /**
     * Get size of this station.
     *
     * @return size.
     */
    public StationSizes getSize() {
        return this.size;
    }
    
    @Override
    public final StructureTypes getStructureType() {
        return StructureTypes.STATION;
    }
    
    /**
     * Get the track related to the station.
     *
     * @return The track bound to the station.
     */
    public final Track getTrack() {
        return this.track;
    }
    
    /**
     * Checks whether the extraction point is the station's business.
     *
     * @param extractionSite
     *            Site to check.
     * @return True iff the site is within radius.
     */
    public final boolean isWithinRadius(final IProductionSite extractionSite) {
        return extractionSite.getX() >= this.getX() - this.getRadius()
            && extractionSite.getX() <= this.getX() + this.getRadius()
            && extractionSite.getY() >= this.getY() - this.getRadius()
            && extractionSite.getY() <= this.getY() + this.getRadius();
    }
    
    /**
     * Gets a list of extraction sites within the station's radius.
     *
     * @param extractionSiteList
     *            List of all sites on the world.
     * @return Filtered list containing only the close ones.
     */
    public final List<IProductionSite> retrieveNearbyExtractionSites(final List<IProductionSite> extractionSiteList) {
        final List<IProductionSite> nearbySites = new ArrayList<IProductionSite>();
        for (final IProductionSite es : extractionSiteList) {
            if (this.isWithinRadius(es)) {
                nearbySites.add(es);
            }
        }
        return nearbySites;
    }
    
    /**
     * Direction for which this station is positioned.
     * 0 - North and South.
     * 1 - West and East.
     * These values latter on are used for choosing correct texture.
     *
     * @param direction
     *            int which represent one of the 2 directions.
     */
    public final void setDirection(final int direction) {
        super.setTexture("STATION_" + this.size.name() + "_" + direction);
    }
    
    /**
     * Returns a list of reachable stations from here.
     *
     * @return list of stations.
     */
    public List<Station> getReachableStations() {
        final List<Station> stations = new ArrayList<Station>();
        for (final Station station : Game.getInstance().getWorld().getStationManager().getStations()) {
            if (station != this && this.canReachStation(station)) {
                stations.add(station);
            }
        }
        return stations;
    }
    
    /**
     * Checks if a station is reachable from here.
     *
     * @param station
     *            other station.
     * @return true or false.
     */
    public boolean canReachStation(final Station station) {
        if (this.equals(station)) {
            throw new IllegalArgumentException("Of course you can reach a station from itself, why do you even ask?");
        }
        final Track otherTrack = station.getTrack();
        return null != otherTrack && null != this.track.getNextTrack(station.getTrack());
        
    }
    
    /**
     * Trade with reachable production sites.
     *
     * @param wagons
     *            list of wagons in a train.
     * @return time the train has to wait in station.
     */
    public double trade(final List<IWagon> wagons) {
        double time = 0;
        for (final IWagon wagon : wagons) {
            for (final IProductionSite productionSite : this.productionSites) {
                time += wagon.loadFrom(productionSite);
            }
        }
        return time;
    }
    
    /**
     *
     */
    public void linkStation() {
        this.linked = true;
    }
    
    /**
     * Tells whether the station is already linked to the tracks.
     * @return True if the station is linked, false otherwise.
     */
    public boolean isLinked() {
        return this.linked;
    }
    
    /**
     * Method that check if a structure can be removed or not.
     * Station can never be removed.
     *
     * @return false because Station can never be removed.
     */
    @Override
    public boolean isRemovable() {
        return false;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public String getHoverText() {
        switch (this.size) {
            case SMALL:
                return "Small Station";
            case MEDIUM:
                return "Medium Station";
            case LARGE:
                return "Large Station";
            default:
                return "Station";
        }
    }
}
