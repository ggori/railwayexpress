package ch.usi.inf.saiv.railwayempire.utilities;

import java.awt.FontFormatException;
import java.awt.Graphics2D;
import java.io.IOException;
import java.io.InputStream;

import org.newdawn.slick.Color;
import org.newdawn.slick.Font;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.UnicodeFont;
import org.newdawn.slick.font.effects.ColorEffect;

/**
 * Simple utility class to support justified text
 * 
 * http://slick.javaunlimited.net/viewtopic.php?t=2640
 * 
 * @author zenzei
 */
public class FontUtils {
    /**
     * Unicode font used for the default rendering.
     */
    public static final UnicodeFont FONT = FontUtils.createFont(SystemConstants.FONT_PATH, SystemConstants.FONT_SIZE,
        SystemConstants.FONT_COLOR);
    
    /**
     * Unicode font used for the rendering of titles and similar important information.
     * Similar to {@link #FONT} but with a larger size.
     */
    public static final UnicodeFont LARGE_FONT = FontUtils.createFont(SystemConstants.FONT_PATH,
        SystemConstants.LARGE_FONT_SIZE, SystemConstants.FONT_COLOR);
    
    /**
     * Draw text left justified
     * 
     * @param font
     *            The font to draw with
     * @param s
     *            The string to draw
     * @param x
     *            The x location to draw at
     * @param y
     *            The y location to draw at
     */
    public static void drawLeft(final Font font, final String s, final int x, final int y) {
        FontUtils.drawString(font, s, Alignment.LEFT, x, y, 0, Color.white);
    }
    
    /**
     * Draw a string
     * 
     * @param font
     *            The font to draw with
     * @param s
     *            The text to draw
     * @param alignment
     *            The alignment to apply
     * @param x
     *            The x location to draw at
     * @param y
     *            The y location to draw at
     * @param width
     *            The width to fill with the string
     * @param color
     *            The color to draw in
     * @return The final x coordinate of the text
     */
    public static int drawString(final Font font, final String s,
        final Alignment alignment, final int x, final int y, final int width,
        final Color color) {
        final int resultingXCoordinate = 0;
        if (alignment == Alignment.LEFT) {
            font.drawString(x, y, s, color);
        } else if (alignment == Alignment.CENTER) {
            font.drawString(x + (width / 2) - (font.getWidth(s) / 2), y, s,
                color);
        } else if (alignment == Alignment.RIGHT) {
            font.drawString(x + width - font.getWidth(s), y, s, color);
        } else if (alignment == Alignment.JUSTIFY) {
            // calculate left width
            final int leftWidth = width - font.getWidth(s);
            if (leftWidth <= 0) {
                // no width left, use standard draw string
                font.drawString(x, y, s, color);
            }
            
            return FontUtils.drawJustifiedSpaceSeparatedSubstrings(font, s, x,
                y, FontUtils.calculateWidthOfJustifiedSpaceInPixels(font,
                    s, leftWidth));
        }
        
        return resultingXCoordinate;
    }
    
    /**
     * Draws justified-space separated substrings based on the given {@link String} and the given starting x and y
     * coordinates to the given {@link Graphics2D} instance.
     * 
     * @param font
     *            The font to draw with
     * @param s
     *            The non-null {@link String} to draw as space-separated
     *            substrings.
     * @param x
     *            The given starting x-coordinate position to use to draw the {@link String}.
     * @param y
     *            The given starting y-coordinate position to use to draw the {@link String}.
     * @param justifiedSpaceWidth
     *            The integer specifying the width of a justified space {@link String}, in pixels.
     * @return The resulting x-coordinate of the current cursor after the
     *         drawing operation completes.
     * @throws NullPointerException
     *             Throws a {@link NullPointerException} if any of the given
     *             arguments are null.
     */
    private static int drawJustifiedSpaceSeparatedSubstrings(final Font font,
        final String s, final int x, final int y,
        final int justifiedSpaceWidth) {
        int curpos = 0;
        int endpos = 0;
        int resultingXCoordinate = x;
        while (curpos < s.length()) {
            endpos = s.indexOf(' ', curpos); // find space
            if (endpos == -1) {
                endpos = s.length(); // no space, draw all string directly
            }
            final String substring = s.substring(curpos, endpos);
            
            font.drawString(resultingXCoordinate, y, substring);
            
            resultingXCoordinate += font.getWidth(substring)
                + justifiedSpaceWidth; // increase
            // x-coordinate
            curpos = endpos + 1;
        }
        
        return resultingXCoordinate;
    }
    
    /**
     * Calculates and returns the width of a single justified space for the
     * given {@link String}, in pixels.
     * 
     * @param font
     *            The font to draw with
     * @param s
     *            The given non-null {@link String} to use to calculate the
     *            width of a space for.
     * @param leftWidth
     *            The integer specifying the left width buffer to use to
     *            calculate how much space a space should take up in
     *            justification.
     * @return The width of a single justified space for the given {@link String}, in pixels.
     */
    private static int calculateWidthOfJustifiedSpaceInPixels(final Font font,
        final String s, final int leftWidth) {
        int space = 0; // hold total space; hold space width in pixel
        int curpos = 0; // current string position
        
        // count total space
        while (curpos < s.length()) {
            if (s.charAt(curpos++) == ' ') {
                space++;
            }
        }
        
        if (space > 0) {
            // width left plus with total space
            // space width (in pixel) = width left / total space
            space = (leftWidth + (font.getWidth(" ") * space)) / space;
        }
        return space;
    }
    
    /**
     * Draws the text to the left.
     * 
     * @param font
     *            The font to use.
     * @param s
     *            The string to draw.
     * @param x
     *            The x coordinate.
     * @param y
     *            The y coordinate.
     * @param color
     *            The color in which to draw.
     */
    public static void drawLeft(final Font font, final String s, final int x, final int y, final Color color) {
        FontUtils.drawString(font, s, Alignment.LEFT, x, y, 0, color);
    }
    
    /**
     * Draw text center justified
     * 
     * @param font
     *            The font to draw with
     * @param s
     *            The string to draw
     * @param x
     *            The x location to draw at
     * @param y
     *            The y location to draw at
     * @param width
     *            The width to fill with the text
     */
    public static void drawCenter(final Font font, final String s, final int x, final int y, final int width) {
        FontUtils.drawString(font, s, Alignment.CENTER, x, y, width, Color.white);
    }
    
    /**
     * Draw text center justified
     * 
     * @param font
     *            The font to draw with
     * @param s
     *            The string to draw
     * @param x
     *            The x location to draw at
     * @param y
     *            The y location to draw at
     * @param width
     *            The width to fill with the text
     * @param color
     *            The color to draw in
     */
    public static void drawCenter(final Font font, final String s, final int x, final int y, final int width,
        final Color color) {
        FontUtils.drawString(font, s, Alignment.CENTER, x, y, width, color);
    }
    
    /**
     * Draw text right justified
     * 
     * @param font
     *            The font to draw with
     * @param s
     *            The string to draw
     * @param x
     *            The x location to draw at
     * @param y
     *            The y location to draw at
     * @param width
     *            The width to fill with the text
     */
    public static void drawRight(final Font font, final String s, final int x, final int y, final int width) {
        FontUtils.drawString(font, s, Alignment.RIGHT, x, y, width, Color.white);
    }
    
    /**
     * Draw text right justified
     * 
     * @param font
     *            The font to draw with
     * @param s
     *            The string to draw
     * @param x
     *            The x location to draw at
     * @param y
     *            The y location to draw at
     * @param width
     *            The width to fill with the text
     * @param color
     *            The color to draw in
     */
    public static void drawRight(final Font font, final String s, final int x, final int y, final int width,
        final Color color) {
        FontUtils.drawString(font, s, Alignment.RIGHT, x, y, width, color);
    }
    
    /**
     * Formats the string as [A-Z][a-z]
     * 
     * @param string
     *            Given string to format.
     * @return Formatted string.
     */
    public static String capitalizeFirstLetter(final String string) {
        return Character.toUpperCase(string.charAt(0)) + string.substring(1).toLowerCase();
    }
    
    /**
     * Font resizing for cool people (i.e. not those who use the AWT Font).
     * 
     * @param font
     *            The font to resize.
     * @param size
     *            The size to resize to.
     * @return The resized font.
     */
    public static UnicodeFont resizeFont(final UnicodeFont font, final float size) {
        final UnicodeFont unicode = new UnicodeFont(font.getFont().deriveFont(size));
        unicode.addNeheGlyphs();
        unicode.getEffects().add(new ColorEffect(java.awt.Color.white));
        try {
            unicode.loadGlyphs();
        } catch (final SlickException exception) {
            exception.printStackTrace();
        }
        return unicode;
    }
    
    /**
     * Actually used to change font size.
     * 
     * @param font
     *            Base font.
     * @param size
     *            New Size.
     * @return Font from the bae and size.
     */
    public static java.awt.Font resizeFont(final java.awt.Font font, final float size) {
        return font.deriveFont(size);
    }
    
    /**
     * Creates a font with the file in path, of size size.
     * 
     * @param path
     *            Path of the font file.
     * @param size
     *            Size of the font.
     * @return Created Font.
     */
    @SuppressWarnings("unchecked")
    public static UnicodeFont createFont(final String path, final float size, final java.awt.Color color) {
        
        UnicodeFont result = new UnicodeFont(new java.awt.Font("Verdana", java.awt.Font.PLAIN, (int) size));
        
        try {
            final InputStream reading = ClassLoader.getSystemResourceAsStream(path);
            
            final java.awt.Font created = java.awt.Font.createFont(java.awt.Font.PLAIN, reading);
            final java.awt.Font real = created.deriveFont(java.awt.Font.TRUETYPE_FONT, size);
            
            result = new UnicodeFont(real);
            result.getEffects().add(new ColorEffect(color));
            result.addNeheGlyphs();
            result.loadGlyphs();
        } catch (final IOException exception) {
            Log.exception(exception);
        } catch (final FontFormatException exception) {
            Log.exception(exception);
        } catch (final SlickException e) {
            Log.exception(e);
        }
        return result;
    }
    
    /**
     * Alignment indicators
     */
    public enum Alignment {
        /**
         * Left alignment
         */
        LEFT,
            /**
             * Center alignment
             */
            CENTER,
            /**
             * Right alignment
             */
            RIGHT,
            /**
             * Justify alignment
             */
            JUSTIFY
    }
}
