/**
 * Provides the application with save, load and high score functionality.
 */
package ch.usi.inf.saiv.railwayempire.utilities.serialization;