package ch.usi.inf.saiv.railwayempire.model.trains;

import ch.usi.inf.saiv.railwayempire.model.wares.IWare;

/**
 * Class used to know what has to be loaded/unloaded from a train at a station.
 */
public final class Trade {
    
    /** the Ware to trade. */
    private final IWare ware;
    /** the quantity. */
    private final int quantity;
    
    /**
     * Constructor.
     * 
     * @param wareToBeTraded
     *        ware to be traded.
     * @param quantityToTrade
     *        quantity to trade.
     */
    public Trade(final IWare wareToBeTraded, final int quantityToTrade) {
        this.ware = wareToBeTraded;
        this.quantity = quantityToTrade;
    }
    
    /**
     * Return the ware to be traded.
     * 
     * @return the ware.
     */
    public IWare getWare() {
        return this.ware;
    }
    
    /**
     * Return the quantity to be traded.
     * 
     * @return the quantity.
     */
    public int getQuantity() {
        return this.quantity;
    }
}
