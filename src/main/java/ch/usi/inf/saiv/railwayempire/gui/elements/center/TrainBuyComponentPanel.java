package ch.usi.inf.saiv.railwayempire.gui.elements.center;

import org.newdawn.slick.geom.Rectangle;

import ch.usi.inf.saiv.railwayempire.gui.elements.AbstractPanel;
import ch.usi.inf.saiv.railwayempire.gui.elements.Label;
import ch.usi.inf.saiv.railwayempire.gui.elements.LayoutPanel;
import ch.usi.inf.saiv.railwayempire.gui.elements.center.components.AbstractComponentPanel;
import ch.usi.inf.saiv.railwayempire.gui.elements.center.components.LocomotiveBuyPanel;
import ch.usi.inf.saiv.railwayempire.gui.elements.center.components.WagonBuyPanel;
import ch.usi.inf.saiv.railwayempire.model.stores.TrainComponentStore;
import ch.usi.inf.saiv.railwayempire.model.trains.Locomotive;
import ch.usi.inf.saiv.railwayempire.model.wagons.IWagon;
import ch.usi.inf.saiv.railwayempire.utilities.FontUtils;
import ch.usi.inf.saiv.railwayempire.utilities.FontUtils.Alignment;
import ch.usi.inf.saiv.railwayempire.utilities.ResourcesLoader;

/**
 * Does nothing.
 */
public final class TrainBuyComponentPanel extends AbstractPanel {
    
    /**
     * The list of all locomotives types.
     */
    private final LayoutPanel locomotivesPanel;
    
    /**
     * The list of all wagons types.
     */
    private final LayoutPanel wagonsPanel;
    
    /**
     * The store from which to get all the train components to display.
     */
    private final TrainComponentStore store;
    
    /**
     * Constructor. fun.
     * 
     * @param rectangle
     *            rectangle, yay.
     */
    public TrainBuyComponentPanel(final Rectangle rectangle) {
        super(rectangle);
        
        this.store = TrainComponentStore.getUniqueInstance();
        
        final Label locomotivesLabel = new Label(this.getX(), this.getY(),
            this.getWidth() / 2, 30, "Locomotives", Alignment.CENTER, true);
        locomotivesLabel.setBackground(ResourcesLoader.getInstance().getImage("BACKGROUND_DARK"));
        locomotivesLabel.setFont(FontUtils.LARGE_FONT);
        this.add(locomotivesLabel);
        
        this.locomotivesPanel = new LayoutPanel(new Rectangle(locomotivesLabel.getX(),
            locomotivesLabel.getY() + locomotivesLabel.getHeight(),
            locomotivesLabel.getWidth(), this.getHeight() - locomotivesLabel.getHeight()),
            LayoutPanel.VERTICAL);
        this.add(this.locomotivesPanel);
        
        final Label wagonsLabel = new Label(locomotivesLabel.getX() + locomotivesLabel.getWidth(), this.getY(),
            this.getWidth() / 2, 30, "Wagons", Alignment.CENTER, true);
        wagonsLabel.setBackground(ResourcesLoader.getInstance().getImage("BACKGROUND_DARK"));
        wagonsLabel.setFont(FontUtils.LARGE_FONT);
        this.add(wagonsLabel);
        
        this.wagonsPanel = new LayoutPanel(new Rectangle(wagonsLabel.getX(),
            wagonsLabel.getY() + wagonsLabel.getHeight(),
            wagonsLabel.getWidth(), this.getHeight() - wagonsLabel.getHeight()),
            LayoutPanel.VERTICAL);
        this.add(this.wagonsPanel);
        
        this.setBackground(ResourcesLoader.getInstance().getImage("BACKGROUND_DARK"));
        
        this.initLocomotivesPanel();
        this.initWagonsPanel();
        
    }
    
    /**
     * Initialize the list of locomotives.
     */
    private void initLocomotivesPanel() {
        for (final Locomotive loc : this.store.getPurchasableLocomotives()) {
            final AbstractComponentPanel panel = new LocomotiveBuyPanel(new Rectangle(0, 0,
                this.locomotivesPanel.getWidth(), AbstractComponentPanel.getIconHeight()), loc);
            panel.setBackground(ResourcesLoader.getInstance().getImage("BACKGROUND"));
            this.locomotivesPanel.add(panel);
        }
        
    }
    
    /**
     * Initialize the list of wagons.
     */
    private void initWagonsPanel() {
        for (final IWagon wagon : this.store.getPurchasableWagons()) {
            final AbstractComponentPanel panel = new WagonBuyPanel(new Rectangle(0, 0,
                this.wagonsPanel.getWidth(), AbstractComponentPanel.getIconHeight()), wagon);
            panel.setBackground(ResourcesLoader.getInstance().getImage("BACKGROUND"));
            this.wagonsPanel.add(panel);
        }
    }
    
}
